@echo off

IF "%1" == "effulgence" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_effulgence
    python do_various.py flats_effulgence
    python do_various.py epub_effulgence
) ELSE IF "%1" == "incandescence" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_incandescence
    python do_various.py flats_incandescence
    python do_various.py epub_incandescence
) ELSE IF "%1" == "sandbox" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_sandbox
    python do_various.py flats_sandbox
    python do_various.py epub_sandbox
) ELSE IF "%1" == "pixiethreads" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_pixiethreads
    python do_various.py flats_pixiethreads
    python do_various.py epub_pixiethreads
) ELSE IF "%1" == "glowfic" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_glowfic
    python do_various.py flats_glowfic
    python do_various.py epub_glowfic
) ELSE IF "%1" == "marri" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_marri
    python do_various.py flats_marri
    python do_various.py epub_marri
) ELSE IF "%1" == "radon" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_radon
    python do_various.py flats_radon
    python do_various.py epub_radon
) ELSE IF "%1" == "peterverse" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_peterverse
    python do_various.py flats_peterverse
    python do_various.py epub_peterverse
) ELSE IF "%1" == "maggie" (
    protoc --python_out=. templates.proto
    python do_various.py tocs_maggie
    python do_various.py flats_maggie
    python do_various.py epub_maggie
) ELSE (
    echo Please pass a parameter for which epub to generate ^(effulgence, incandescence, sandbox, pixiethreads, glowfic, marri, radon, peterverse or maggie^)
    echo Otherwise, to be more specific in which function to run, perform ^"python3 do_various.py^" and pick an option there
)