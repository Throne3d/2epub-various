#!/bin/bash
case $1 in
    "") cat <<EOF
Please pass a parameter for which epub to generate (effulgence, incandescence, sandbox, pixiethreads, glowfic, marri, radon, peterverse or maggie).
Otherwise, to be more specific in which function to run, perform "python3 do_various.py" and pick an option there.
EOF
        exit;
        ;;
    effulgence)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_effulgence
        python3 do_various.py flats_effulgence
        python3 do_various.py epub_effulgence
        ;;
    incandescence)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_incandescence
        python3 do_various.py flats_incandescence
        python3 do_various.py epub_incandescence
        ;;
    sandbox)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_sandbox
        python3 do_various.py flats_sandbox
        python3 do_various.py epub_sandbox
        ;;
    pixiethreads)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_pixiethreads
        python3 do_various.py flats_pixiethreads
        python3 do_various.py epub_pixiethreads
        ;;
    glowfic)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_glowfic
        python3 do_various.py flats_glowfic
        python3 do_various.py epub_glowfic
        ;;
    marri)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_marri
        python3 do_various.py flats_marri
        python3 do_various.py epub_marri
        ;;
    radon)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_radon
        python3 do_various.py flats_radon
        python3 do_various.py epub_radon
        ;;
    peterverse)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_peterverse
        python3 do_various.py flats_peterverse
        python3 do_various.py epub_peterverse
        ;;
    maggie)
        protoc --python_out=. templates.proto
        python3 do_various.py tocs_maggie
        python3 do_various.py flats_maggie
        python3 do_various.py epub_maggie
        ;;
    *) echo "Unknown collection - run without parameters for more details."
esac