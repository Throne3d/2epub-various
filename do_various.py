#!/usr/bin/python3
from bs4 import BeautifulSoup
from bs4 import Tag
from bs4 import NavigableString
from ebooklib import epub
import urllib, urllib.parse, urllib.request, re, os, shutil, traceback, sys, pyratemp, time, datetime
from templates_pb2 import Chapters, Chapter, Face, Post

logOutput = True

def get_file(url, where="web_cache", savePath="", replace=True, output=False, retryCount=0, maxRetry=3):
    if (where == ""):
        where = "."
    url = url.replace("https://", "http://")
    if (not url[:7] == "http://"):
        raise ValueError("The URL passed [{}] wasn't an HTTP URL.".format(url))
    if (maxRetry % 1 != 0 or maxRetry < 0):
        raise ValueError("The maxRetry parameter must be a positive integer. [Was: {} [type: {}]]".format(maxRetry, type(maxRetry)))
    url = strip_url_fragment(url)
    
    if (savePath == ""):
        filename = make_fileName_safe(where + "/" + url.replace("http://", ""))
        if (filename[-1:] == "/"):
            filename = filename + "index"
        dir = "/".join(filename.split("/")[:-1])
    else:
        filename = savePath
        dir = "/".join(savePath.split("/")[:-1])
    
    got = False
    try:
        if (dir != "." and not os.path.isdir(dir)):
            os.makedirs(dir)
        if (not os.path.isfile(filename) or replace):
            with urllib.request.urlopen(url) as response, open(filename, mode="wb") as out_file:
                shutil.copyfileobj(response, out_file)
                got = True
    except urllib.error.HTTPError as e:
        if (retryCount != maxRetry):
            output_msg("--- Error loading URL {}. Retrying ({} of {}). [Error type was: {}; Reason: {}] ---".format(url, retryCount+1, maxRetry, e.code, e.reason))
            ret = get_file(url=url, where=where, savePath=savePath, replace=replace, output=output, retryCount=retryCount+1, maxRetry=maxRetry)
            return ret
        else:
            #traceback.print_exc()
            output_msg("--- Failed to load URL: {}. ---".format(url))
    except OSError as e:
        output_msg("--- Failed to load URL: {}. ---".format(url))
        output_msg("--- Error type: {}; {} ---".format(e.errno, e.strerror))
        traceback.print_exc()
    else:
        if (output and got):
            output_msg("Got file [{}]".format(url))
        elif (output):
            output_msg("Didn't get file [{}] (maybe it already exists and we're not replacing it).".format(url))
        return filename

def make_fileName_safe(fileName):
    #\/:*?"<>|
    return fileName.replace("\\", "~×BACKSLASH×~").replace(":", "~×COLON×~").replace("*", "~×ASTERISK×~").replace("?", "~×QMARK×~").replace("\"", "~×QUOT×~").replace("<", "~×LT×~").replace(">", "~×GT×~").replace("|", "~×BAR×~")

def reverse_fileName_safe(fileName):
    return fileName.replace("~×BACKSLASH×~", "\\").replace("~×COLON×~", ":").replace("~×ASTERISK×~", "*").replace("~×QMARK×~", "?").replace("~×QUOT×~", "\"").replace("~×LT×~", "<").replace("~×GT×~", ">").replace("~×BAR×~", "|")

def strip_url_fragment(url):
    url = urllib.parse.urldefrag(url)[0]
    if (url[-1:] == "#"):
        url = url[:-1]
    return url

def set_params_in_url(url, *args, **kargs):
    if (not len(args) % 2 == 0):
        raise ValueError("Please give a URL then index,value pairs to replace in the query string.")
    
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qsl(urlSplit.query)
    
    for i in range(0, int(len(args)/2)):
        params.append((args[i*2], args[i*2+1]))
    for karg in kargs:
        params.append((karg,kargs[karg]))
    
    params.sort() #Prevent weird ordering of query string (results in duplicate files)
        
    new_urlSplit = urlSplit._replace(query=urllib.parse.urlencode(params, doseq=True))
    return urllib.parse.urlunsplit(new_urlSplit)

def clear_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    new_urlSplit = urlSplit._replace(query=[])
    return urllib.parse.urlunsplit(new_urlSplit)

def set_url_params(url, *args, **kargs):
    return set_params_in_url(clear_params_in_url(url), *args, **kargs)

def get_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    return params

def get_param_in_url(url, *args):
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    if (len(args) == 1): 
        if (args[0] in params and len(params[args[0]]) > 0):
            return params[args[0]][0]
        elif args[0] in params:
            return params[args[0]]
        else:
            return None
    ret = []
    for arg in args:
        if (arg in params and len(params[arg]) > 0):
            ret.append(params[arg][0])
        elif (arg in params):
            ret.append(params[arg])
        else:
            ret.append(None)
    return ret

def remove_control_characters(html):
    def str_to_int(s, default, base=10):
        if int(s, base) < 0x10000:
            return unichr(int(s, base))
        return default
    html = re.sub(r"&#(\d+);?", lambda c: str_to_int(c.group(1), c.group(0)), html)
    html = re.sub(r"&#[xX]([0-9a-fA-F]+);?", lambda c: str_to_int(c.group(1), c.group(0), base=16), html)
    html = re.sub(r"[\x00-\x08\x0b\x0e-\x1f\x7f]", "", html)
    return html

outputFileName = None
prevMaintLine = False
def output_msg(message="", *things, end="\n", maintainLine=False):
    global outputFileName, logOutput, prevMaintLine
    
    if (maintainLine):
        end = ""
    elif (prevMaintLine):
        message = "\n" + message
    prevMaintLine = maintainLine
    print(message, *things, end=end)
    if (not logOutput):
        return
    message = str(message)
    things = [str(thing) for thing in things]
    if (outputFileName == None):
        #outputFileName = set_output_settings()
        print("No outputFileName.")
    if (not os.path.isdir("logs")):
        os.makedirs("logs")
    with open("logs/" + outputFileName, mode="a") as outputFile:
        outputFile.write(message + (" " if len(things) > 0 else "") + " ".join(things) + end)

def set_output_settings(process="epub", group="effulgence"):
    global outputFileName
    outputFileName = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H %M") + " " + process + "_" + group + ".log"

def make_chapter(url, name, path=None, nameExtras="", sectionType="", thread="", section="", sectionExtras="", pageCount=0, fullyLoaded=0):
    chapterDetails = Chapter()
    chapterDetails.url = url
    chapterDetails.smallURL = url.replace("http://", "").replace(".dreamwidth.org", "").replace("?style=site&view=flat", "").replace("?view=flat&style=site", "").replace(".html", "")
    if (path == None):
        path = get_file(url, replace=False)
    chapterDetails.path = path
    chapterDetails.name = name
    chapterDetails.nameExtras = nameExtras
    chapterDetails.sectionType = sectionType
    chapterDetails.thread = thread
    chapterDetails.section = section
    chapterDetails.sectionExtras = sectionExtras
    chapterDetails.pageCount = pageCount
    chapterDetails.fullyLoaded = fullyLoaded #The page we got to the last time we fully loaded it (i.e. the last page of the chapter, provided we fully generated the HTML into "output/")
    
    output_msg("\"{}\": {}".format("{} {}".format(chapterDetails.name, chapterDetails.nameExtras) if chapterDetails.nameExtras != "" else chapterDetails.name, chapterDetails.smallURL))
    
    return chapterDetails

noonMatcher = re.compile(r"((\d|\d{2})(:|.)(\d{2})) ?(am|pm)")
def noonFix(time):
    #Acts funny with times like "24:13pm" (12:13?), since they're utter nonsense. Otherwise seems good.
    global noonMatcher
    noonMatch = noonMatcher.search(time)
    if (noonMatch):
        groups = noonMatch.groups()
        timeMatch = time[noonMatch.start():noonMatch.end()]
        
        noonAP = groups[4].lower()
        hour = int(groups[1]) % 24
        minutes = int(groups[3]) % 60
        separator = groups[2]
        replaceString = timeMatch
        if (hour == 12 and noonAP == "am"):
            hour = 0
        elif (hour == 12 and noonAP == "pm"):
            hour = 12
        elif (noonAP == "am"):
            pass
        elif (noonAP == "pm" and hour < 12):
            hour = hour + 12
        elif (noonAP == "pm" and hour < 24):
            pass
        else:
            return time
        replaceString = "{}:{}".format("0" + str(hour) if hour < 10 else str(hour), "0" + str(minutes) if minutes < 10 else str(minutes))
        time = time.replace(timeMatch, replaceString)
    return time

def make_comment(author, content, time, id, chapter, cache, postTitle="", depth=0, parent=None, postType="comment"):
    global noonMatcher
    if (postType.lower().strip() == "comment"):
        postType = Post.COMMENT
    elif (postType.lower().strip() == "entry"):
        postType = Post.ENTRY
    else:
        raise ValueError("Invalid postType [{}]".format(postType))
    if (parent == None and postType != Post.ENTRY):
        parent = chapter.entry
    
    if (type(author) == Face):
        author = author.user + ": " + author.face
    
    commentDetails = None
    if (postType == Post.ENTRY):
        commentDetails = chapter.entry
        del commentDetails.childrenIDs[:]
    if (parent != None and type(parent) == Post):
        parent.childrenIDs.extend(["{}#{}".format(author, id)])
    if (commentDetails == None):
        commentDetails = Post()
        #Not an entry
    
    if (type(chapter) == Chapter):
        chapter = chapter.url + "#" + chapter.thread
    if (type(parent) == Post and parent != None):
        parent = "{}#{}".format(parent.author, parent.id)
    elif (parent == None):
        parent = "" #Probably because it's an entry. This was (by accident) "#0" by default before.
    
    time = noonFix(time)
    
    commentDetails.author = author
    commentDetails.content = content
    commentDetails.time = time
    commentDetails.id = id
    commentDetails.chapter = chapter
    commentDetails.depth = depth
    commentDetails.parent = parent
    commentDetails.postTitle = postTitle
    commentDetails.postType = postType
    
    cache["posts"]["{}#{}".format(commentDetails.author, commentDetails.id)] = commentDetails
    
    return commentDetails

def get_chapter_from_string(str, cache):
    if (str in cache["chapters"]):
        return cache["chapters"][str]
    else:
        return None

def get_post_from_string(str, cache):
    if (str in cache["posts"]):
        return cache["posts"][str]
    else:
        output_msg("Failed to find a post with uid [{}]".format(str))
        return None

def get_post_from_authorid(author, id, cache):
    return get_post_from_string("{}#{}".format(author, id), cache)

def make_entry(**kargs):
    return make_comment(postType="entry", parent=None, depth=0, **kargs)

def load_chapters(chapterGroup="effulgence", where=""):
    if (where == ""):
        where = "web_cache/chapterDetails_" + chapterGroup + ".txt"
    if (not os.path.isfile(where)):
        return []
    else:
        chapters = []
        with open(where, "rb") as file:
            chapterString = file.read()
        chapterRep = Chapters()
        chapterRep.ParseFromString(chapterString)
        if (chapterRep == None):
            return []
        for chapter in chapterRep.chapters:
            chapters.append(chapter)
        return chapters

def save_chapters(chapters, chapterGroup="effulgence", where=""):
    if (where == ""):
        where = "web_cache/chapterDetails_" + chapterGroup + ".txt"
    chapterRep = Chapters()
    for chapter in chapters:
        chapterRep.chapters.extend([chapter])
    with open(where, "wb") as file:
        file.write(chapterRep.SerializeToString())

def get_prev_chapter_lengths(chapterGroup="effulgence"):
    chapterList = load_chapters(chapterGroup)
    prevLengths = {}
    for chapter in chapterList:
        if (chapter.pageCount != 0):
            prevLengths[chapter.url] = chapter.pageCount
    return prevLengths

def get_prev_full_loads(chapterGroup="effulgence"):
    chapterList = load_chapters(chapterGroup)
    prevLoads = {}
    for chapter in chapterList:
        if (chapter.pageCount != 0):
            prevLoads[chapter.url] = chapter.fullyLoaded
    return prevLoads

def shorten_long_sections(section):
    #Literally a mapping of long section names (e.g. "Miscellaneous/Other Short Form Sandboxes") to shorter ones (e.g. "Other Shorts")
    #Currently used for console output purposes only.
    return section.replace("Miscellaneous/Other Short Form Sandboxes", "Other Shorts").replace("Adventures of Demon Cam", "Demon Cam").replace("Non-Juliet Slayer Bells", "Other Slayers").replace("Misadventures of Holly and Crystal", "Holly and Crystal")

def generate_comment_html(comment, commentFace, cache):

    chapterDetails = get_chapter_from_string(comment.chapter, cache)
    
    #Moiety - Dreamwidth, incandescence & effulgence, specific
    #commentFace = get_face_from_string(comment.author, cache)
    
    html = cache["commentTemplate"](**{"id":comment.id, "depth":comment.depth, "postType":"ENTRY" if comment.postType == Post.ENTRY else "COMMENT", "moiety":commentFace.moiety, "postTitle":comment.postTitle, "imagePath":commentFace.imagePath, "userDisplay":commentFace.userDisplay, "face":commentFace.face, "user":commentFace.user, "content":comment.content, "chapterName":chapterDetails.name, "nameExtras":chapterDetails.nameExtras, "timestamp":comment.time,"entryAuthors":""})
        
    return html

def add_image(localImageURL, cache):
    if (localImageURL == None or localImageURL == ""):
        return localImageURL
    #localImageURL e.g. ../images/v-dreamwidth-org-111111-1111.png or output/images/...
    if (localImageURL[:3] == "../"):
        localImageURL = localImageURL.replace("../", "output/")
    if (localImageURL in cache["images"]):
        return cache["images"][localImageURL]
    
    imgBookID = localImageURL.replace("output/images/", "images/").replace("/", "-")
    
    cache["images"][localImageURL] = localImageURL.replace("output/images/", "../images/")
    
    img = epub.EpubItem(uid=imgBookID, file_name=cache["images"][localImageURL].replace("../images/", "images/"))
    with open(localImageURL, mode="rb") as imag:
        img.set_content(imag.read())
    cache["book"].add_item(img)
    return cache["images"][localImageURL]

def get_collections(collectionList=[], output=True):
    with open("collectionPages.txt", mode="r") as collections:
        for collection in collections:
            collection = collection.replace("\r\n", "").replace("\n", "")
            if (collection.strip() != "" and not collection.find(" ~#~ ") == -1):
                collectionName = collection.split(" ~#~ ")[0].strip()
                collectionURL = collection.split(" ~#~ ")[1].strip()
                #They're all in the format http://PROFILENAME.dreamwidth.org/ (or should be)
                
                if (output):
                    output_msg("Processing collection {}.".format(collectionName))
                
                collectionPage = get_file(collectionURL)
                
                with open(collectionPage) as collectionFile:
                    collectionSoup = BeautifulSoup(collectionFile)
                
                if not (collectionName in collectionList):
                    collectionList[collectionName] = []
                
                collectionProfileName = collectionURL.lower().replace("http://", "").replace("https://", "").replace(".dreamwidth.org/profile", "")
                if (not collectionProfileName in collectionList[collectionName]):
                    collectionList[collectionName].append(collectionProfileName)
                
                #Each collection will have a class of "collectionnametolower" - e.g. "Alicorn ~#~ http://belltower.dreamwidth.org/profile" will make any members have the class "alicorn" (CSS selected by ".alicorn")
                
                members = collectionSoup.select("div#members_people_body a")
                for member in members:
                    memberURL = member["href"].replace("https://", "http://").replace("/profile", "/").strip()
                    if (memberURL != ""):
                        memberName = memberURL.replace("http://", "").replace(".dreamwidth.org/", "").replace(".dreamwidth.org", "").strip().lower()
                        if (not memberName in collectionList[collectionName]):
                            collectionList[collectionName].append(memberName)
                
                if (output):
                    output_msg("Found {} members.".format(len(collectionList[collectionName])))
    return collectionList

def get_local_imagePath(imageURL, cache):
    if (imageURL == ""):
        return ""
    imageURL = imageURL.strip()
    if (imageURL in cache["images"]):
        return cache["images"][imageURL]
    localURL = "output/images/" + make_fileName_safe(imageURL.replace("https://", "http://").replace("http://", "").replace("/", "-"))
    if (localURL in cache["images"]):
        return cache["images"][localURL]
    
    imageExts = [".gif", ".png", ".jpg", ".jpeg", ".tif", ".tiff"]
    hasExt = False
    for imageExt in imageExts:
        if (localURL.rfind(imageExt) >= len(localURL) - len(imageExt)):
            hasExt = True
    if (not hasExt):
        localURL += ".png"
    
    internal_URL = get_file(imageURL, savePath=localURL, replace=False)
    return add_image(internal_URL, cache)

def internalise_content(content, cache):
    #If anyone uses "&lt" anywhere as regular text, this will cause it to become "<", potentially erroneously. Be careful.
    if (type(content) is str):
        content = content.replace("&amp;lt", "&lt;").replace("&amp;gt", "&gt;")
        content = BeautifulSoup(content)
    else:
        content = BeautifulSoup(str(content).replace("&amp;lt", "&lt;").replace("&amp;gt", "&gt;"))
    for image in content.select("img"):
        if ("src" in image.attrs and image.attrs["src"].strip() != ""):
            image["src"] = image["src"].strip().lower().replace("https://", "http://")
            imageURL = image["src"]
            if (imageURL.find("http://") != -1):
                image["src"] = get_local_imagePath(imageURL, cache)
    return content

def get_face_from_post(post, cache, addImages=True, doIcons=True):
    collectionList = cache["collections"]
    userPic = post.select("div.userpic")[0]
    
    faceImgURL = ""
    faceImgPath = ""
    faceName = ""
    userID = post.select("span.ljuser")
    if (userID):
        userID = userID[0]
        username = userID["lj:user"].strip().lower()
        userID = userID.select("a")
        if (len(userID) > 0 and "href" in userID[0].attrs):
            userID = userID[0]["href"].replace("https://", "http://").replace("http://", "").replace("/profile", "/").replace(".dreamwidth.org/", "").replace(".dreamwidth.org", "")
        else:
            userID = username
            output_msg("Using fallback profileID (username: {})".format(username))
            #It can be that userID is e.g. "subtly-artistic" but username is "subtly_artistic"
            #So when they're not both available... just, there might be need to be careful. Hence, fallback.
    else:
        output_msg("FAILED to find a username for a post in this chapter. Post HTML: {}".format(post.prettify()))
        author = Face()
        return author
    faceImg = userPic.find("img")
    if (userPic.a and userPic.a["href"] != "" and faceImg and "title" in faceImg.attrs):
        faceName = faceImg["title"].split(username + ": ")
        if (len(faceName) > 1):
            faceName = ": ".join(faceName[1:])
        else:
            output_msg("Couldn't find a face name for username [{}] and image title [{}]?".format(username, faceImg["title"]))
            faceName = ""
        faceName = faceName.strip()
    defaultImg = ""
    if (faceImg and "src" in faceImg.attrs and faceImg["src"].strip() != ""):
        defaultImg = faceImg["src"]
    author = get_face_from_string(userID + ": " + faceName, cache, username=username, defaultImg=defaultImg, addImages=addImages, doIcons=doIcons)
    return author

def get_face_from_string(str, cache, username="", defaultImg="", addImages=True, doIcons=True):
    global nameChanges
    if (str in cache["faces"]):
        return cache["faces"][str]
    else:
        collectionList = cache["collections"]
        
        if (len(str.split(": ")) <= 1):
            return None
        
        userID = str.split(": ")[0].strip()
        faceName = ": ".join(str.split(": ")[1:]).strip()
        
        if (username == ""):
            username = userID
        
        prevID = userID
        for fromName in nameChanges:
            if (username.lower() == fromName.lower() or userID.lower() == fromName.lower()):
                userID = nameChanges[fromName]
                username = userID.replace("-", "_")
        
        if (defaultImg[:8] == "https://"):
            defaultImg = "http://" + defaultImg[8:]
        faceImgURL = defaultImg
        faceImgPath = ""
        
        if (faceName != "" and doIcons):
            if (not userID in cache["iconPages"]):
                iconPageURL = "http://{}.dreamwidth.org/icons?style=site".format(userID)
                iconPagePath = get_file(iconPageURL)
                cache["iconPages"][userID] = {}
                if (not iconPagePath):
                    output_msg("--- Failed to get the icon page for {} ---".format(userID))
                else:
                    nextPagePath = iconPagePath
                    while (nextPagePath):
                        with open(nextPagePath) as nextPage:
                            soup = BeautifulSoup(nextPage)
                        icons = soup.select(".icon-container .icon")
                        for icon in icons:
                            iconNames = icon.select(".icon-info .icon-keywords ul li")
                            iconNames.extend(icon.select(".icon-info .default"))
                            for desctext in icon.select(".icon-info .icon-description .description-text"):
                                desctext.decompose()
                            iconDescs = []
                            for iconDesc in icon.select(".icon-info .icon-description"):
                                iconDescText = iconDesc.text.strip().lower()
                                if (not iconDescText in iconDescs):
                                    iconDescs.append(iconDescText)
                                else:
                                    iconDescs = []
                                    output_msg("--- Duplicate descriptions for some icons in {} ---".format(userID))
                                    break
                            if (len(iconNames) == 0):
                                output_msg("--- Failed to find a keyword for a face in {} ---".format(userID))
                            
                            iconURL = icon.select(".icon-image img")
                            if (len(iconURL) < 1):
                                output_msg("--- Couldn't find a face for [{}] ---".format(userID))
                                iconURL = ""
                            else:
                                if ("src" in iconURL[0].attrs):
                                    iconURL = iconURL[0]["src"].replace("https://", "http://").strip().lower()
                                else:
                                    iconURL = ""
                            
                            for iconNameElement in iconNames:
                                iconName = iconNameElement.text.strip().split(" (")[0].lower().strip()
                                if (iconName[-1:] == ","):
                                    iconName = iconName[:-1]
                                cache["iconPages"][userID][iconName] = iconURL
                            for iconDesc in iconDescs:
                                cache["iconPages"][userID][iconDesc] = iconURL
                        pageNext = soup.select("#content .action-box .page-next a")
                        if (len(pageNext) > 0 and "href" in pageNext[0].attrs and pageNext[0]["href"].strip() != ""):
                            nextPagePath = get_file(pageNext[0]["href"].strip())
                        else:
                            nextPagePath = None
            if (faceName[:9].lower() == "(default)"):
                faceNameFind = "default"
            else:
                faceNameFind = faceName.lower()
            if (not faceNameFind in cache["iconPages"][userID]):
                faceNameDesc = faceNameFind.find("(") >= 0 and faceNameFind.split("(")[1].split(")")[0].strip()
                if (faceNameDesc and faceNameDesc in cache["iconPages"][userID]):
                    faceImgURL = cache["iconPages"][userID][faceNameDesc]
                else:
                    #output_msg("--- Failed to find a URL for '{}: {}' ---".format(userID, faceName))
                    faceImgURL = defaultImg
            else:
                faceImgURL = cache["iconPages"][userID][faceNameFind]
        elif (not doIcons):
            faceImgPath = ""
        if (faceImgURL[:7] == "http://"):
            if (addImages):
                faceImgPath = get_local_imagePath(faceImgURL, cache)
            else:
                faceImgPath = faceImgURL
        else:
            faceImgPath = faceImgURL
        if (faceImgPath == None):
            if (userID in cache["iconPages"] and "default" in cache["iconPages"][userID]):
                failedFaceImgURL = faceImgURL
                faceImgURL = cache["iconPages"][userID]["default"]
                if (faceImgURL[:7] == "http://"):
                    if (addImages):
                        faceImgPath = get_local_imagePath(faceImgURL, cache)
                    else:
                        faceImgPath = faceImgURL
                else:
                    faceImgPath = faceImgURL
                if (faceImgPath != None):
                    output_msg("-- Failed to load image for {}: {}. Using default [{}] instead of [{}]".format(userID, faceName, faceImgURL, failedFaceImgURL))
        if ((faceImgPath == "" or faceImgPath == None) and faceName != "" and doIcons):
            cache["failedFaces"].append("{}#{}".format(userID, faceName))
            output_msg("--- Failed to find face on icons page: '{}: {}' ---".format(userID, faceName))
        
        if (not userID + ": " + faceName in cache["faces"]):
            author = Face()
            author.user = userID
            author.userDisplay = username.strip()
            author.imageURL = faceImgURL
            author.moiety = ""
            if (author.user in cache["failedMoieties"]):
                author.moiety = ""
            else:
                for collectionName in collectionList:
                    collection = collectionList[collectionName]
                    for memberName in collection:
                        if (memberName.replace("_", "-") == author.user.replace("_", "-")):
                            author.moiety += re.sub("[^A-Za-z0-9_]", "", collectionName.lower()) + " "
                author.moiety = author.moiety.strip()
                if (author.moiety == ""):
                    output_msg("Couldn't find a moiety for {}".format(author.user))
                    output_msg("Was trying to match: {}".format(author.user.strip().lower()))
                    cache["failedMoieties"].append(author.user)
            
            author.imagePath = faceImgPath.strip()
            author.face = faceName.strip()
            cache["faces"][userID + ": " + faceName] = author
            if (prevID != userID):
                cache["faces"][prevID + ": " + faceName] = author
        else:
            author = cache["faces"][userID + ": " + faceName]
        return author

def do_chapter_html(chapter, chapterPath, cache, skipCache=False):
    if (not skipCache):
        filePath = "output/{}-{}.html".format(cache["group"], strip_url_fragment(chapter.path).replace("web_cache/", "").split(make_fileName_safe("?"))[0].replace(".html", "") + ("~-~" + chapter.thread if chapter.thread != "" else ""))
        html = None
        if (chapter.fullyLoaded == chapter.pageCount and os.path.isfile(filePath)):
            with open(filePath, "r") as savedHTML:
                html = savedHTML.read()
            if (len(html.strip()) > 0 and (html.find("class=\"section-title\"") < 0 or html.find(chapter.section) >= 0)):
                #It's not blank, and it's got the section-title right if applicable.
                return html, True
    if (chapter.url.find("dreamwidth.org") >= 0):
        chapterContents = [] #e.g. "entry, cmt-264, cmt-265, branchNote1, cmt-266, cmt-267, branchNote2, cmt-268" which gives
        # entry
        #  cmt-264
        #   cmt-265
        #   branchNote1
        #    cmt-266
        #     cmt-267
        #   branchNote2
        #    cmt-268
        #divList["264"] = '<div blah>' (cmt-264's HTML)
        
        postList = {}
        divList = {}
        
        with open(chapterPath) as first_flat_file:
            soup = BeautifulSoup(first_flat_file)
        
        warning = soup.select("h1.text-center")
        if (len(warning) > 0):
            warning = warning[0]
            warningExists = warning.text.find("Discretion Advised")
            if (warningExists != None and warningExists != -1):
                output_msg("FAILED to load chapter {}-{}.".format(chapter.section, chapter.name))
                output_msg("Please re-download the chapter and all its flat pages - it's got a discretion advised message.")
                return None
        
        entry = soup.select("div.entry")
        if (len(entry) < 1):
            output_msg("ERROR with chapter! (No entry?)")
            return None
        entry = entry[0]
        
        if (chapter.pageCount == 0):
            page_list = soup.find("div", class_="comment-page-list")
            if (page_list):
                page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
            else:
                page_count = 1
            chapter.pageCount = page_count
        
        author = get_face_from_post(entry, cache)
        
        authorList = []
        authorListString = ""
        
        if (cache["showAuthors"]):
            tagList = entry.select(".tag ul li a")
            for tagElement in tagList:
                tagText = tagElement.text
                nextAuthor = ""
                if (tagText[:7] == "person:"):
                    nextAuthor = tagText[7:]
                elif (tagText[:7] == "author:"):
                    nextAuthor = tagText[7:]
                nextAuthor = nextAuthor.strip()
                if (nextAuthor != "" and not nextAuthor in authorList):
                    authorList.append(nextAuthor)
            i = -1
            for authorTag in authorList:
                i += 1
                if (i == 0):
                    authorListString += authorTag
                elif (i == len(authorList)-1):
                    authorListString += " and " + authorTag
                else:
                    authorListString += ", " + authorTag
            authorListString = authorListString.strip()
        
        content = entry.select("div.entry-content")[0]
        content = str(internalise_content(content, cache).div)
        time = entry.select("span.datetime")[0].text.strip()
        id = chapter.path.split(".dreamwidth.org/")[1].split(".")[0]
        postTitle = soup.select("h3.entry-title")[0].text
        entry = make_entry(author=author.user + ": " + author.face, content=content, time=time, id=id, chapter=chapter, postTitle=postTitle, cache=cache)
        postList["0"] = entry
        
        entryHTML = generate_comment_html(entry, author, cache)
        divList["0"] = entryHTML
        chapterContents.append("entry")
        
        threadStart = chapter.thread
        if (threadStart != ""): threadStart = threadStart
        else: threadStart = "0"
        inThread = [threadStart]
        
        for pageNum in range(1, chapter.pageCount+1):
            currentPageURL = strip_url_fragment(set_url_params(chapter.url, **{"view":"flat", "style":"site", "page":pageNum}).replace("https://", "http://"))
            with open(make_fileName_safe(currentPageURL.replace("http://", "web_cache/")), mode="r") as currPage:
                currPageSoup = BeautifulSoup(currPage)
            pageWarning = currPageSoup.find("h1", class_="text-center")
            if (pageWarning):
                pageWarningExists = pageWarning.text.find("Discretion Advised")
                if (pageWarningExists != None and pageWarningExists != -1):
                    output_msg("--- FAILED to load page {} of chapter {}-{}. WILL NOT PROCESS ANY COMMENTS OF THIS CHAPTER. ---".format(pageNum, chapter.section, chapter.name))
                    break
            
            comments = currPageSoup.select("div.comment")
            if (len(comments) < 1):
                output_msg("This page has no comments.")
                if (chapter.pageCount > 1):
                    output_msg("--- There's an issue with the page of URL {}. The entry has no comments, but the number of flat pages is greater than 1. ---".format(currentPageURL))
                
            for comment in comments:
                if (not comment):
                    output_msg("--- Comment doesn't exist? ---")
                    continue
                id = "id" in comment.attrs and len(comment["id"].split("comment-cmt")) > 1 and comment["id"].split("comment-cmt")[1]
                content = comment.select("div.comment-content")
                if (len(content) > 0):
                    content = str(internalise_content(content[0], cache).div)
                
                if not id:
                    output_msg("--- Error on page {} ---".format(currChapterURL))
                    output_msg("--- Couldn't find a comment ID. (ID: {})".format("" if not "id" in comment.attrs else comment["id"]))
                    continue
                if not content:
                    output_msg("--- Error on page {} ---".format(currChapterURL))
                    output_msg("--- Couldn't find comment content. (ID: {}; Comment: {})".format(id, comment))
                    continue
                
                
                parentLink = comment.select("li.commentparent")
                parentID = "0"
                if (len(parentLink) > 0):
                    parentID = parentLink[0].a["href"].split("#cmt")[1]
                if (inThread[0] == id):
                    #It's the start of the thread, actually. "Parent" is entry.
                    parentID = "0"
                
                depth = 1
                
                if (not (id in inThread or parentID in inThread)):
                    #Not in the "thread" list... Skip.
                    continue
                
                parentPost = parentID in postList and postList[parentID] #parent "Post"
                parentDiv = parentID in divList and divList[parentID] #parent "div" (BeautifulSoup)
                
                if (parentDiv and parentPost):
                    depth = parentPost.depth + 1
                else:
                    output_msg("--- No div with id {} ? ---".format(str(parentID)))
                    output_msg("--- We're on pageNum {} which has the URL {} (page {} of {} from {})".format(pageNum, currentPageURL, pageNum, chapter.pageCount, chapter.name))
                    output_msg("--- Curr contents: {}".format(chapterContents))
                    output_msg("--- Unknown depth, too. Assuming 1.")
                    output_msg("--- Will probably error soon...")
                    depth = 1
                    parentID = "0"
                    parentPost = postList[parentID]
                    parentDiv = parentID in divList and divList[parentID]
                
                author = get_face_from_post(comment, cache)
                
                time = comment.select("span.datetime")[0].text.strip()
                
                comment = make_comment(author=author.user + ": " + author.face, content=content, time=time, id=id, chapter=chapter, postTitle=postTitle, depth=depth, parent=parentPost, cache=cache)
                postList[id] = comment
                
                commentHTML = generate_comment_html(comment, author, cache)
                divList[id] = commentHTML
                parentI = chapterContents.index("cmt-{}".format(parentID) if parentID != "0" else "entry")
                chapterContents.insert(parentI+1, "cmt-{}".format(id))
                
                commentParent = get_post_from_string(comment.parent, cache)
                
                if (len(commentParent.childrenIDs) > 1):
                    #We've got siblings! Such fun!
                    siblings = commentParent.childrenIDs
                    
                    lastSibling = None
                    for i in reversed(siblings):
                        if (i != "{}#{}".format(comment.author, comment.id)):
                            lastSibling = get_post_from_string(i, cache)
                    
                    if (lastSibling == None):
                        output_msg("--- Apparently has siblings, but doesn't have non-self siblings? [Post ID: {}; Siblings: {}] ---".format(comment.id, siblings))
                    else:
                        output_msg("- Found a branch.")
                    
                        latestDesc = lastSibling
                        while (len(latestDesc.childrenIDs) > 0):
                            latestDesc = get_post_from_string(latestDesc.childrenIDs[-1], cache)
                        #latestDesc should be the latest (Post) descendent of the previous sibling (i.e. put the new HTML next).
                        
                        chapterContents.remove("cmt-{}".format(id)) #Remove the previous copy of this
                        chapterContents.insert(chapterContents.index("cmt-{}".format(latestDesc.id))+1, "cmt-{}".format(id))
                        #Put cmt-[id] right after cmt-[latestDescendant]
                        
                        if (len(siblings) == 2):
                            #We've just created the first branch thing! (Be nice to the reader; tell them there's a branch.)
                            if (lastSibling.postType == Post.ENTRY):
                                lastSiblingI = chapterContents.index("entry") #Should be 0.
                                #Really shouldn't be the last sibling? Entries shouldn't have siblings.
                                output_msg("--- Error: The previous sibling really shouldn't be an entry. ---")
                            else:
                                if (not "cmt-{}".format(lastSibling.id) in chapterContents):
                                    output_msg("- Couldn't find the previous sibling (cmt-{}) in chapterContents?: {}".format(lastSibling.id, chapterContents))
                                lastSiblingI = chapterContents.index("cmt-{}".format(lastSibling.id))
                            #Put branchNote1 just before the previous sibling
                            chapterContents.insert(lastSiblingI, "branchNote1")
                            
                            #Put branchNote2 just before the newly-added post.
                            chapterContents.insert(chapterContents.index("cmt-{}".format(id)), "branchNote2")
                        else:
                            #Put branchNote{i} just after the latest descendant
                            chapterContents.insert(chapterContents.index("cmt-{}".format(latestDesc.id))+1, "branchNote{}".format(len(siblings)))
                if (not id in inThread):
                    inThread.append(id)
        
        index = -1
        for content in chapterContents:
            index += 1
            if (content == "branchNote1"):
                parentCommentContent = chapterContents[index-1]
                if (not "cmt-" in parentCommentContent):
                    if (parentCommentContent != "entry"): #Only warn the user if the non-comment is not an entry (i.e. only complain about branch-notes in succession).
                        output_msg("--- Branch note has a non-comment just before it. ---")
                    continue
                parentCommentID = parentCommentContent.split("cmt-")[1]
                if (not parentCommentID in postList):
                    #output_msg("--- Branch note's parent is not in the post list. ---")
                    continue
                parentComment = postList[parentCommentID]
                if (len(parentComment.childrenIDs) != 2):
                    #Has more than 2 branches - probably not a redundant branch
                    continue
                firstChild = get_post_from_string(parentComment.childrenIDs[0], cache)
                secondChild = get_post_from_string(parentComment.childrenIDs[1], cache)
                if (len(firstChild.childrenIDs) != 0 and len(secondChild.childrenIDs) != 0):
                    #Not redundant.
                    continue
                #One of them is redundant. Remove the branch note.
                branchNote1I = chapterContents.index("cmt-{}".format(firstChild.id)) - 1
                branchNote2I = chapterContents.index("cmt-{}".format(secondChild.id)) - 1
                if (not "branchNote" in chapterContents[branchNote1I]):
                    #One of them isn't a branch-note.
                    output_msg("--- First child of cmt-{}, cmt-{}, doesn't have the branch-note it should. ---".format(parentComment.id, firstChild.id))
                    continue
                elif (not "branchNote" in chapterContents[branchNote2I]):
                    output_msg("--- Second child of cmt-{}, cmt-{}, doesn't have the branch-note it should. ---".format(parentComment.id, secondChild.id))
                    continue
                chapterContents.pop(chapterContents.index("cmt-{}".format(firstChild.id)) - 1)
                chapterContents.pop(chapterContents.index("cmt-{}".format(secondChild.id)) - 1)
                index = chapterContents.index("cmt-{}".format(parentComment.id))
                output_msg("- Removed redundant branch-note from cmt-{}.".format(parentComment.id))
        
        html = cache["chapterTemplate"](sectionTitle=chapter.section, sectionTitleExtras=chapter.sectionExtras, entry=divList["0"], chapter={"name":chapter.name, "nameExtras":chapter.nameExtras, "authors":authorListString, "postTitle":postTitle}, chapterContents=chapterContents, divList=divList)
    
    return html, False

wordCountRegex = re.compile("\w+")
def process_chapter_stats(chapter, chapterPath, cache):
    global wordCountRegex
    if (chapter.url.find("dreamwidth.org") >= 0):
        postList = {}
        
        entryStartDates = cache["stats"]["entryStartDates"]
        characterCounts = cache["stats"]["characterCounts"]
        authorCounts = cache["stats"]["authorCounts"]
        postWordCounts = cache["stats"]["postWordCounts"]
        
        with open(chapterPath) as first_flat_file:
            soup = BeautifulSoup(first_flat_file)
        
        warning = soup.select("h1.text-center")
        if (len(warning) > 0):
            warning = warning[0]
            warningExists = warning.text.find("Discretion Advised")
            if (warningExists != None and warningExists != -1):
                output_msg("FAILED to load chapter {}-{}.".format(chapter.section, chapter.name))
                output_msg("Please re-download the chapter and all its flat pages - it's got a discretion advised message.")
                return None
        
        entry = soup.select("div.entry")
        if (len(entry) < 1):
            output_msg("ERROR with chapter! (No entry?)")
            return None
        entry = entry[0]
        
        if (chapter.pageCount == 0):
            page_list = soup.find("div", class_="comment-page-list")
            if (page_list):
                page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
            else:
                page_count = 1
            chapter.pageCount = page_count
        
        author = get_face_from_post(entry, cache, addImages=False, doIcons=False)
        
        content = entry.select("div.entry-content")[0]
        content = content.text
        
        time = entry.select("span.datetime")[0].text.strip()
        id = chapter.path.split(".dreamwidth.org/")[1].split(".")[0]
        postTitle = soup.select("h3.entry-title")[0].text
        entry = make_entry(author=author.user + ": " + author.face, content="", time=time, id=id, chapter=chapter, postTitle=postTitle, cache=cache) #Content ignored for speed. Probably.
        postList["0"] = entry
        
        #Do statistics on entry here.
        if (time and len(time) >= 7):
            if (not time[:7] in entryStartDates):
                entryStartDates[time[:7]] = []
            entryStartDates[time[:7]].append(id)
        if (author and author.user and len(author.user) > 0):
            if (not author.user in characterCounts):
                characterCounts[author.user] = 0
            characterCounts[author.user] += 1
        if (author and author.moiety and len(author.moiety) > 0):
            if (not author.moiety in authorCounts):
                authorCounts[author.moiety] = 0
            authorCounts[author.moiety] += 1
        
        
        entryWordCount = len(wordCountRegex.findall(content))
        if (not chapter.smallURL in postWordCounts):
            postWordCounts[chapter.smallURL] = {"authors":{}, "users":{}, "_total":0, "chapter":chapter}
        
        postWordCounts[chapter.smallURL]["_total"] += entryWordCount
        
        if (author and author.user and len(author.user) > 0):
            if (not author.user in postWordCounts[chapter.smallURL]["users"]):
                postWordCounts[chapter.smallURL]["users"][author.user] = 0
            postWordCounts[chapter.smallURL]["users"][author.user] += entryWordCount
        
        if (author and author.moiety and len(author.moiety) > 0):
            if (not author.moiety in postWordCounts[chapter.smallURL]["authors"]):
                postWordCounts[chapter.smallURL]["authors"][author.moiety] = 0
            postWordCounts[chapter.smallURL]["authors"][author.moiety] += entryWordCount
        #End statistics
        
        threadStart = chapter.thread
        if (threadStart != ""): threadStart = threadStart
        else: threadStart = "0"
        inThread = [threadStart]
        
        for pageNum in range(1, chapter.pageCount+1):
            currentPageURL = strip_url_fragment(set_url_params(chapter.url, **{"view":"flat", "style":"site", "page":pageNum}).replace("https://", "http://"))
            with open(make_fileName_safe(currentPageURL.replace("http://", "web_cache/")), mode="r") as currPage:
                currPageSoup = BeautifulSoup(currPage)
            pageWarning = currPageSoup.find("h1", class_="text-center")
            if (pageWarning):
                pageWarningExists = pageWarning.text.find("Discretion Advised")
                if (pageWarningExists != None and pageWarningExists != -1):
                    output_msg("--- FAILED to load page {} of chapter {}-{}. WILL NOT PROCESS ANY COMMENTS OF THIS CHAPTER. ---".format(pageNum, chapter.section, chapter.name))
                    break
            
            comments = currPageSoup.select("div.comment")
            if (len(comments) < 1):
                output_msg("This page has no comments.")
                if (chapter.pageCount > 1):
                    output_msg("--- There's an issue with the page of URL {}. The entry has no comments, but the number of flat pages is greater than 1. ---".format(currentPageURL))
                
            for comment in comments:
                if (not comment):
                    output_msg("--- Comment doesn't exist? ---")
                    continue
                id = "id" in comment.attrs and len(comment["id"].split("comment-cmt")) > 1 and comment["id"].split("comment-cmt")[1]
                
                if not id:
                    output_msg("--- Error on page {} ---".format(currChapterURL))
                    output_msg("--- Couldn't find a comment ID. (ID: {})".format("" if not "id" in comment.attrs else comment["id"]))
                    continue
                
                parentLink = comment.select("li.commentparent")
                parentID = "0"
                if (len(parentLink) > 0):
                    parentID = parentLink[0].a["href"].split("#cmt")[1]
                if (inThread[0] == id):
                    #It's the start of the thread, actually. "Parent" is entry.
                    parentID = "0"
                
                depth = 1
                
                if (not (id in inThread or parentID in inThread)):
                    #Not in the "thread" list... Skip.
                    continue
                
                parentPost = parentID in postList and postList[parentID] #parent "Post"
                
                if (parentPost):
                    depth = parentPost.depth + 1
                else:
                    output_msg("--- No div with id {} ? ---".format(str(parentID)))
                    output_msg("--- We're on pageNum {} which has the URL {} (page {} of {} from {})".format(pageNum, currentPageURL, pageNum, chapter.pageCount, chapter.name))
                    output_msg("--- Unknown depth, too. Assuming 1.")
                    output_msg("--- Will probably error soon...")
                    depth = 1
                    parentID = "0"
                    parentPost = postList[parentID]
                
                author = get_face_from_post(comment, cache, addImages=False, doIcons=False)
                
                time = comment.select("span.datetime")[0].text.strip()
                
                content = comment.select("div.comment-content")
                if (len(content) > 0):
                    content = content[0].text
                else:
                    content = ""
                
                comment = make_comment(author=author.user + ": " + author.face, content="", time=time, id=id, chapter=chapter, postTitle=postTitle, depth=depth, parent=parentPost, cache=cache) #Content ignored for speed. Again.
                postList[id] = comment
                
                #Do comment statistics here
                if (author and author.user and len(author.user) > 0):
                    if (not author.user in characterCounts):
                        characterCounts[author.user] = 0
                    characterCounts[author.user] += 1
                if (author and author.moiety and len(author.moiety) > 0):
                    if (not author.moiety in authorCounts):
                        authorCounts[author.moiety] = 0
                    authorCounts[author.moiety] += 1
                
                commentWordCount = len(wordCountRegex.findall(content))
                if (not chapter.smallURL in postWordCounts):
                    postWordCounts[chapter.smallURL] = {"authors":{}, "users":{}, "_total":0, "chapter":chapter}
                
                postWordCounts[chapter.smallURL]["_total"] += commentWordCount
                
                if (author and author.user and len(author.user) > 0):
                    if (not author.user in postWordCounts[chapter.smallURL]["users"]):
                        postWordCounts[chapter.smallURL]["users"][author.user] = 0
                    postWordCounts[chapter.smallURL]["users"][author.user] += commentWordCount
                
                if (author and author.moiety and len(author.moiety) > 0):
                    if (not author.moiety in postWordCounts[chapter.smallURL]["authors"]):
                        postWordCounts[chapter.smallURL]["authors"][author.moiety] = 0
                    postWordCounts[chapter.smallURL]["authors"][author.moiety] += commentWordCount
                #End comment statistics here
                
                commentParent = get_post_from_string(comment.parent, cache)
                
                if (len(commentParent.childrenIDs) > 1):
                    #We've got siblings! Such fun!
                    pass
                if (not id in inThread):
                    inThread.append(id)
        cache["stats"]["entryStartDates"] = entryStartDates
        cache["stats"]["characterCounts"] = characterCounts
        cache["stats"]["authorCounts"] = authorCounts
        cache["stats"]["postWordCounts"] = postWordCounts

blockElements = ["address", "article", "aside", "blockquote", "br", "canvas", "dd", "div", "dl", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "li", "main", "nav", "noscript", "ol", "output", "p", "pre", "section", "table", "tfoot", "ul", "video"]
def get_text_on_line_in(element, root=True):
    global blockElements
    theText = ""
    stopped = False
    if (type(element) is Tag):
        if (element.name.lower().strip() in blockElements):
            stopped = True
        else:
            for content in element.contents:
                text, stop = get_text_on_line_in(content, False)
                theText += text
                if (stop):
                    stopped = True
                    break
    else:
        theText += str(element)
    if (root):
        return theText
    else:
        return theText, stopped

def get_text_on_line(element):
    theText = ""
    stopped = False
    nextElement = element
    while (nextElement != None):
        text, stop = get_text_on_line_in(nextElement, False)
        theText += text
        if (stop):
            stopped = True
            break
        nextElement = nextElement.next_sibling
    return theText

collectionList = {
    "Adalene":["lurkingkobold", "wish-i-may"],
    "Adiva":["gothamsheiress", "adivasheadvoices"],
    "Ajzira":["lost-in-translation", "hearing-shadows"],
    "AndaisQ":["fortheliving", "quite-enchanted", "andomega", "in-like-a", "hemomancer", "white-ram", "power-in-the", "strangely-literal", "sonofsnow", "dontbelieveinfairies"],
    "Anthusiasm":["queenoftrash"],
    "armokGoB":["armokgob"],
    "Benedict":["unblinkered", "penitencelost"],
    "Calima":["tenn-ambar-metta"],
    "Ceitfianna":["balancingminds", "mm-ceit"],
    "ChristyHotwater":["slgemp141"],
    "CuriousDiscoverer":["mage-see-mage-do", "abodyinmotion", "superego-medico", "not-without-scars", "breeds-contempt", "curiousdiscoverer", "come-forth-winter", "copycast", "of-all-trades", "ignite-the-light", "there-is-no-such-thing-as", "unadalturedstrength", "tailedmonstrosity", "curiousbox"], #Bluelantern
    "Endovior":["withmanyfingers"],
    "ErinFlight":["thrown-in", "regards-the-possibilities", "back-from-nowhere", "vive-la-revolution"],
    "Eva":["kaolinandbone", "evesystem", "all-the-worlds-have", "walksonmusic", "eternally-aggrieved"], #evenstar?
    "Kel":["kelardry", "dotted-lines"], #BlueSkySprite
    "kuuskytkolme":["can-i-help", "can-i-stay", "can-i-go"],
    "Link":["meletiti-entelecheiai", "chibisilian"], #chibisilian is assumed from "Location: Entelechy"
    "Liz":["sun-guided"],
    "Lynette":["darkeningofthelight", "princeofsalem"],
    "Maggie":["maggie-of-the-owls", "whatamithinking", "iamnotpolaris", "amongstherpeers", "amongstthewinds", "asteptotheright", "jumptotheleft", "themainattraction", "swordofdamocles", "swordofeden", "feyfortune", "mutatis-mutandis", "mindovermagic", "ragexserenity"],
    "Nemo":["magnifiedandeducated", "connecticut-yankee", "unprophesied-of-ages", "nemoconsequentiae", "wormcan", "off-to-be-the-wizard", "whole-new-can"],
    "roboticlin":["roboticlin"],
    "Rockeye":["witchwatcher", "rockeye-stonetoe", "sturdycoldsteel", "characterquarry", "allforthehive", "neuroihive", "smallgod"],
    "Sigma":["spiderzone"], #Ezra
    "Teceler":["scatteredstars", "onwhatwingswedareaspire"],
    "TheOneButcher":["theonebutcher"],
    "Timepoof":["timepoof"],
    "Unbitwise":["unbitwise", "wind-on-my-face", "synchrosyntheses"],
    "Verdancy":["better-living", "forestsofthe"],
    "Yadal":["yorisandboxcharacter", "kamikosandboxcharacter"],
    "Zack":["intomystudies"]
    #, "Unknown":["ambrovimvor", "hide-and-seek", "antiprojectionist", "vvvvvvibrant", "botanical-engineer", "fine-tuned"]
}

nameChanges = {
    #"FROM":"TO", ...
    "revivificar":"revivian"
}

allowedDomains = [".dreamwidth.org"]

def main(args):
    global collectionList
    if (len(args) < 1):
        print("Please input an argument (e.g. 'tocs_sandbox', 'flats_sandbox', 'epub_sandbox', or 'remove alicorn*#1640' to remove all 1640.html within any alicorn* community)")
        sys.exit()
    
    if (not os.path.isdir("web_cache")):
        os.makedirs("web_cache")
    
    option = " ".join(args).lower().strip()
    process = ""
    group = ""
    if (option[:4] == "tocs"):
        process = "tocs"
    elif (option[:5] == "flats"):
        process = "flats"
    elif (option[:4] == "epub"):
        process = "epub"
    elif (option[:3] == "det"):
        process = "det" #Output the details of the chapters
    elif (option[:5] == "clean"):
        process = "clean"
    elif (option[:6] == "remove"):
        process = "remove"
    elif (option[:4] == "stat"):
        process = "stats"
    else:
        print("Unknown option. Please try with a valid option (call with no parameters to see some examples).")
        sys.exit()
    
    showAuthors = False
    if (process != "remove"):
        if (option[-10:] == "effulgence"):
            group = "effulgence"
        elif (option[-13:] == "incandescence"):
            group = "incandescence"
        elif (option[-7:] == "sandbox"):
            group = "sandbox"
            showAuthors = True
        elif (option[-12:] == "pixiethreads"):
            group = "pixiethreads"
        elif (option[-12:] == "othersandbox" or option[-8:] == "sandbox2" or option[-7:] == "glowfic"):
            group = "glowfic"
            showAuthors = True
        elif (option[-5:] == "marri" or option[-11:] == "marrinikari"):
            group = "marri"
            showAuthors = True
        elif (option[-5:] == "radon" or option[-8:] == "absinthe"):
            group = "radon-absinthe"
        elif (option[-5:] == "peter" or option[-10:] == "peterverse"):
            group = "peterverse"
        elif (option[-6:] == "maggie" or option[-15:] == "maggieoftheowls"):
            group = "maggie"
        else:
            print("Unknown thing to download. Please try with a valid option (call with no parameters to see some examples).")
            sys.exit()
    else:
        group = ""

    set_output_settings(process=process, group=group if group != "" else "remove")
    output_msg("-" * 60)
    if (process == "tocs"):
        #Doing ToCs for sandbox
        chapterList = []
        prevChapterLengths = get_prev_chapter_lengths(group)
        prevLoads = get_prev_full_loads(group)
        
        #Sandboxes
        if (group == "sandbox"):
            toc = "http://alicornutopia.dreamwidth.org/1640.html?style=site"
        elif (group == "glowfic"):
            toc = "http://glowfic.dreamwidth.org/2015/06/"
        #Stories
        elif (group == "effulgence"):
            toc = "http://edgeofyourseat.dreamwidth.org/2121.html?style=site"
        elif (group == "incandescence"):
            toc = "http://alicornutopia.dreamwidth.org/7441.html?style=site"
        elif (group == "pixiethreads"):
            toc = "http://pixiethreads.dreamwidth.org/613.html?style=site"
        elif (group == "radon-absinthe"):
            toc = "http://radon-absinthe.dreamwidth.org/295.html?style=site"
        #People
        elif (group == "marri"):
            toc = "http://marrinikari.dreamwidth.org/1634.html?style=site"
        elif (group == "peterverse"):
            toc = "http://peterverse.dreamwidth.org/1643.html?style=site"
        elif (group == "maggie"):
            toc = "http://maggie-of-the-owls.dreamwidth.org/454.html?style=site"
        
        output_msg("Parsing TOCs (of {}).".format(group))
        if (group != "glowfic"):
            output_msg("TOC Page: {}".format(toc))
        
        
        prevData = load_chapters(group)
        if (prevData):
            save_chapters(prevData, group, where="web_cache/oldChapterDetails_" + group + ".txt")
        
        if (group != "incandescence"):
            with open(get_file(toc)) as tocFile:
                soup = BeautifulSoup(tocFile.read())
        else:
            #This seriously works? Ugh. Fix incandescence to not parse stupidly. Cool, now it can parse like effulgence.
            with open(get_file(toc)) as tocFile:
                soup = BeautifulSoup(tocFile.read().replace("</li>", ""))
        
        if (group != "glowfic"):
            #Get a list of chapter links from each TOC
            entry = soup.select(".entry-content")[0]
        
        #Get a list of chapter links from each TOC
        if (group == "sandbox"):
            #Hopefully this'll be squashed into "peterverse".
            #First separator ("<b>SHORT FORM SANDBOXES</b>")
            separators = ["SHORT FORM SANDBOXES", "MULTI-THREAD PLOTS", "IN WHICH I GUEST STAR"]
            storyType = 0
            
            chapterURL = ""
            chapterName = ""
            chapterExtras = ""
            chapterSection = ""
            chapterSectionExtras = ""
            chapterSectionExtrasDone = False
            sectionType = ""
            cont = False
            for em in entry.findAll("em"):
                em.unwrap()
                #Remove Alicorn's favourite italics, since they complicate the DOM quite a bit.
            
            loadChapter = False
            for content in entry.contents:
                for separator in separators:
                    contentText = (content.text if type(content) is Tag else content if type(content) is NavigableString else "")
                    if (contentText.find(separator) != None and contentText.find(separator) != -1):
                        storyType += 1
                        if (storyType == 2):
                            loadChapter = True
                        sectionType = separator
                        chapterSection = separator #To change from the previous section, if there was one.
                        output_msg("Story type change. (Now in {} - {})".format(storyType, separator))
                        cont = True
                if (cont):
                    cont = False
                    continue
                if (storyType == 1 or storyType == 3):
                    #Short form. (or "IN WHICH I GUEST STAR", which is treated the same.)
                    if (content.name == "br" or content.name == "a" or (content.name == "strong" and content.find("a"))):
                        #It's a linebreak. Process the previous content.
                        loadChapter = True
                    if (content.name == "b" or content.name == "u"):
                        chapterSection = content.text
                        output_msg("New section: {} (story type: {})".format(chapterSection, sectionType))
                    elif (content.name == "a"):
                        chapterURL = content["href"].replace("https://", "http://")
                        chapterName = content.text.strip()
                        if (chapterURL.find("dreamwidth.org") == -1):
                            #invalid chapter! (Not dreamwidth; other sites aren't being handled yet)
                            chapterName = ""
                            chapterURL = ""
                            chapterExtras = ""
                    elif (content.name == "strong" and content.find("a")):
                        chapterURL = content.find("a")["href"].replace("https://", "http://")
                        chapterName = content.find("a").text.strip()
                        chapterName += " *"
                        content.find("a").extract()
                        chapterExtras = content.text.strip()
                    elif (chapterURL != ""):
                        chapterExtras += (str(content) if type(content) is Tag else content if type(content) is NavigableString else "")
                if (loadChapter):
                    loadChapter = False
                    if (chapterName != "" and chapterURL != ""):
                        if (chapterName != ""):
                            chapterThread = get_param_in_url(chapterURL, "thread") or ""
                            chapterURL = set_url_params(chapterURL, **{"style":"site","view":"flat"})
                            chapterExtras = chapterExtras.strip()
                            
                            if (chapterURL in prevChapterLengths):
                                prevChapterLength = prevChapterLengths[chapterURL]
                            else:
                                prevChapterLength = 0
                            if (chapterURL in prevLoads):
                                prevLoad = prevLoads[chapterURL]
                            else:
                                prevLoad = 0
                            
                            #Currently, chapterName ends in " *" if it's a finished (bolded) link. Fix it so it's inverted
                            if (chapterName[-2:] == " *"):
                                chapterName = chapterName[:-2]
                            else:
                                chapterName += " +"
                            
                            chapterDetails = make_chapter(url=chapterURL, name=chapterName, nameExtras=chapterExtras, sectionType=sectionType, thread=chapterThread, section=chapterSection, sectionExtras=chapterSectionExtras, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                            
                            chapterList.append(chapterDetails)
                            save_chapters(chapterList, group)
                            
                            chapterURL = ""
                            chapterName = ""
                            chapterExtras = ""
                if (storyType == 2):
                    #Multi-thread story.
                    if (content.name == "br"):
                        #It's a linebreak. Process the previous content.
                        if (chapterURL == "" and chapterSection != "" and not chapterSectionExtrasDone):
                            chapterSectionExtrasDone = True
                    elif (content.name == "i"):
                        chapterSection = content.text
                        chapterSectionExtras = ""
                        chapterSectionExtrasDone = False
                    elif (content.name == "ol" or content.name == "ul"):
                        chapterSoup = BeautifulSoup(str(content))
                        for link in chapterSoup.select("a"):
                            chapterURL = link["href"].replace("https://", "http://")
                            chapterName = link.text.strip()
                            if (chapterURL.find("dreamwidth.org") == -1):
                                chapterName = ""
                                chapterURL = ""
                            if (chapterName != "" and chapterURL != ""):
                                if (chapterName != ""):
                                    chapterThread = get_param_in_url(chapterURL, "thread") or ""
                                    chapterURL = set_url_params(chapterURL, **{"style":"site","view":"flat"})
                                    chapterExtras = chapterExtras.strip()
                                    
                                    if (chapterURL in prevChapterLengths):
                                        prevChapterLength = prevChapterLengths[chapterURL]
                                    else:
                                        prevChapterLength = 0
                                    if (chapterURL in prevLoads):
                                        prevLoad = prevLoads[chapterURL]
                                    else:
                                        prevLoad = 0
                                    
                                    if (chapterName[-2:] == " *"):
                                        chapterName = chapterName[:-2]
                                    #else:
                                    #    #Not finished
                                    #    chapterName += " +"
                                    #Not used in multi-thread plots.
                                    
                                    chapterDetails = make_chapter(url=chapterURL, name=chapterName, nameExtras=chapterExtras, sectionType=sectionType, thread=chapterThread, section=chapterSection, sectionExtras=chapterSectionExtras, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                                    
                                    chapterList.append(chapterDetails)
                                    save_chapters(chapterList, group)
                                    
                                    chapterURL = ""
                                    chapterName = ""
                                    chapterExtras = ""
                    elif (chapterURL == "" and chapterSection != "" and not chapterSectionExtrasDone):
                        chapterSectionExtras += (str(content) if type(content) is Tag else content if type(content) is NavigableString else "")
            
            if (chapterURL.find("dreamwidth.org") < 0):
                chapterName = ""
                chapterURL = ""
            if (chapterName != "" and chapterURL != ""):
                chapterThread = get_param_in_url(chapterURL, "thread") or ""
                chapterURL = set_url_params(chapterURL, **{"style":"site","view":"flat"})
                chapterExtras = chapterExtras.strip()
                
                if (chapterURL in prevChapterLengths):
                    prevChapterLength = prevChapterLengths[chapterURL]
                else:
                    prevChapterLength = 0
                if (chapterURL in prevLoads):
                    prevLoad = prevLoads[chapterURL]
                else:
                    prevLoad = 0
                
                if (chapterName[-2:] == " *"):
                    chapterName = chapterName[:-2]
                #else:
                #    #Not finished
                #    chapterName += " +"
                #Not used in multi-thread plots.
                
                chapterDetails = make_chapter(url=chapterURL, name=chapterName, nameExtras=chapterExtras, sectionType=sectionType, thread=chapterThread, section=chapterSection, sectionExtras=chapterSectionExtras, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                
                chapterList.append(chapterDetails)
                
                chapterURL = ""
                chapterName = ""
                chapterExtras = ""
                save_chapters(chapterList, group)
        elif (group == "effulgence" or group == "pixiethreads" or group == "incandescence" or group == "radon-absinthe"):
            #The template whereby each group of threads is ordered. 
            #Happens in Effulgence with "1. make a wish > 1. ✴ he couldn't have imagined"
            sections = entry.select("> ol > li")
            i = 0
            for section in sections:
                i += 1
                sectionChapter = False
                chapters = section.select("ol > li > a")
                if (len(chapters) < 1):
                    #This section is probably a chapter all by itself.
                    chapters = section.select("> a")
                    sectionChapter = True
                
                sectionTitle = ""
                for word in section.contents:
                    haystack = str(word).replace(" ", "")
                    if ("<br>" in haystack or "<br/>" in haystack or "<ol>" in haystack or "<ul>" in haystack):
                        break
                    sectionTitle += word.text if type(word) is Tag else str(word)
                sectionTitle = re.sub(r"\<[\s\S]*?\>", "", sectionTitle).strip()
                
                if (sectionTitle == ""):
                    sectionTitle = str(i)
                
                if (len(chapters) < 1):
                    output_msg("Error: Couldn't find any chapters for \"{}\"".format(sectionTitle))
                
                output_msg("Section: {}".format(sectionTitle))
                for link in chapters:
                    #Find the chapter's URL (it's the link); for dreamwidth, we "flatten" (more later) this and make it use the site's style.
                    chapterThread = get_param_in_url(link["href"], "thread") or ""
                    chapterURL = set_url_params(link["href"], **{"style": "site", "view": "flat"})
                    
                    #Find the chapter's name (e.g. "1. make a wish > 1.1. [] he couldn't have imagined", finds "he couldn't have imagined")
                    
                    if (sectionChapter):
                        chapterName = sectionTitle
                    else:
                        chapterName = ""
                        for word in link.parent.contents:
                            haystack = str(word).replace(" ", "")
                            if ("<br>" in haystack or "<br/>" in haystack or "<ol>" in haystack or "<ul>" in haystack):
                                break
                            chapterName += word.text if type(word) is Tag else str(word)
                        chapterName = re.sub(r"\<[\s\S]*?\>", "", chapterName).strip()
                    
                    #Add it to the list
                    if (chapterURL in prevChapterLengths):
                        prevChapterLength = prevChapterLengths[chapterURL]
                    else:
                        prevChapterLength = 0
                    if (chapterURL in prevLoads):
                        prevLoad = prevLoads[chapterURL]
                    else:
                        prevLoad = 0
                    
                    chapterDetails = make_chapter(url=chapterURL, name=chapterName, thread=chapterThread, section=sectionTitle, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                    chapterList.append(chapterDetails)
                save_chapters(chapterList, group)
        elif (group == "glowfic"):
            #The template whereby you eat the archives of a community. Nom.
            #(Goes through the months of archives)
            defaultCont = "no continuity"
            chapterList = {} #{"continuity": [chapter1, chapter2]}
            
            while toc != "":
                toc = toc.replace("https://", "http://")
                
                tocPath = get_file(toc)
                output_msg("TOC Page: {}".format(toc))
                with open(tocPath, mode="r") as tocFile:
                    soup = BeautifulSoup(tocFile)
                
                nextPageLinks = soup.select(".navigation .month-forward a")
                if (len(nextPageLinks) > 0):
                    nextPageLink = nextPageLinks[0]
                    if ("href" in nextPageLink.attrs):
                        toc = nextPageLink["href"].strip()
                    else:
                        toc = ""
                else:
                    toc = ""
                toc = toc.strip()
                if (len(toc.replace("https://", "http://").replace("http://glowfic.dreamwidth.org/", "").split("/")) > 1 and toc[-1:] != "/"):
                    toc += "/"
                
                entries = soup.select("#archive-month .month .entry-title")
                for entry in entries:
                    chapterTitleEle = entry.select("a")
                    if (len(chapterTitleEle) < 1):
                        continue
                    chapterTitleEle = chapterTitleEle[0]
                    chapterTitle = chapterTitleEle.text.strip()
                    chapterURL = "href" in chapterTitleEle.attrs and chapterTitleEle["href"]
                    if (not chapterURL):
                        #output_msg("--- NO URL FOR ENTRY. ---")
                        continue
                    chapterURL = set_url_params(chapterURL, **{"style": "site", "view": "flat"})
                    chapterTags = entry.findNext("div", class_="tag")
                    if (not chapterTags):
                        output_msg("Failed to find tags for entry {}".format(chapterTitle))
                        continue
                    chapterTags = chapterTags.select("ul li a")
                    chapterSection = defaultCont
                    skip = False
                    complete = False
                    for tag in chapterTags:
                        tagText = tag.text.strip()
                        if (tagText.lower()[:11] == "continuity:"):
                            chapterSection = tagText[12:].strip()
                        if (tagText.lower()[:5] == "meta:"):
                            skip = True
                            break
                        if (tagText.lower()[:7] == "status:"):
                            if (tagText.lower().find(": complete") >= 0):
                                complete = True
                    if (skip):
                        continue
                    if (not complete):
                        chapterTitle += " +"
                    if (chapterURL in prevChapterLengths):
                        prevChapterLength = prevChapterLengths[chapterURL]
                    else:
                        prevChapterLength = 0
                    if (chapterURL in prevLoads):
                        prevLoad = prevLoads[chapterURL]
                    else:
                        prevLoad = 0
                    
                    chapterDetails = make_chapter(url=chapterURL, name=chapterTitle, section=chapterSection, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                    if (not chapterSection in chapterList):
                        chapterList[chapterSection] = []
                    chapterList[chapterSection].append(chapterDetails)
            conts = sorted(chapterList.keys())
            sortedChapterList = []
            for cont in conts:
                if (cont == defaultCont or cont.lower() == "oneshot"):
                    continue
                for chapter in chapterList[cont]:
                    sortedChapterList.append(chapter)
            for cont in conts:
                if (cont == defaultCont or cont.lower() == "oneshot"):
                    for chapter in chapterList[cont]:
                        sortedChapterList.append(chapter)
            
            save_chapters(sortedChapterList, group)
        elif (group == "marri" or group == "maggie"):
            #The template Marri uses. It's the one with "<strong>Collections</strong> (with extras)"
            #Happens with "Lumos (with Alicorn) > 1. back to school shopping"
            entryContents = entry.contents
            OLs = entry.select("> ol")
            ULs = entry.select("> ul")
            OLi = 0
            ULi = 0
            sections = []
            while (OLi < len(OLs) or ULi < len(ULs)):
                if (OLi >= len(OLs)):
                    sections.extend(ULs[ULi:])
                    ULi = len(ULs)
                    continue
                elif (ULi >= len(ULs)):
                    sections.extend(OLs[OLi:])
                    OLi = len(OLs)
                    continue
                if (OLs[OLi] in ULs[ULi].find_previous_siblings("ol")):
                    sections.append(OLs[OLi])
                    OLi += 1
                    continue
                if (ULs[ULi] in OLs[OLi].find_previous_siblings("ul")):
                    sections.append(ULs[ULi])
                    ULi += 1
                    continue
                output_msg("--- ERROR ordering the lists.")
                sections = OLs
                sections.extend(ULs)
                break
            i = 0
            for section in sections:
                i += 1
                sectionTitle = ""
                uberSectionTitle = ""
                prevBoldU = [x for x in section.find_previous_siblings("u") if len(x.select("strong")) > 0 or len(x.select("b")) > 0] or [x for x in section.find_previous_siblings("strong") or section.find_previous_siblings("b") if len(x.select("u")) > 0]
                if (prevBoldU and len(prevBoldU) > 0):
                    prevBoldU = prevBoldU[0]
                prevStrong = section.find_previous_sibling("strong") or section.find_previous_sibling("b")
                if (prevStrong != None):
                    prevList = None
                    if (i > 1 and i - 2 < len(sections)):
                        prevList = sections[i-2]
                    if (prevList != None):
                        if (not prevStrong in prevList.find_next_siblings("strong") and not prevStrong in prevList.find_next_siblings("b")):
                            #It's not after the previous list.
                            #Therefore, doesn't apply.
                            prevStrong = None
                
                if (prevBoldU):
                    uberSectionTitle = get_text_on_line(prevBoldU).strip()
                if (prevStrong):
                    sectionTitle = get_text_on_line(prevStrong).strip()
                
                if (sectionTitle == ""):
                    sectionTitle = str(i)
                
                sectionDisplayThings = [uberSectionTitle, sectionTitle]
                sectionDisplayThings = filter(None, sectionDisplayThings)
                sectionDisplay = " > ".join(sectionDisplayThings)
                #sectionDisplay = "{} > {}".format(uberSectionTitle, sectionTitle) if uberSectionTitle and sectionTitle else "{}".format(sectionTitle) if not uberSectionTitle and sectionTitle else "{}".format(uberSectionTitle) sectionTitle
                
                chapters = section.select("> li > a")
                if (len(chapters) < 1):
                    output_msg("Error: Couldn't find any chapters for \"{}\"".format(sectionDisplay))
                    continue
                
                output_msg("Section: {}".format(sectionDisplay))
                for link in chapters:
                    li = link.parent
                    chapterThread = get_param_in_url(link["href"], "thread") or ""
                    chapterURL = set_url_params(link["href"], **{"style": "site", "view": "flat"})
                    
                    allowedDom = False
                    for allowedDomain in allowedDomains:
                        if (chapterURL.find(allowedDomain) >= 0):
                            allowedDom = True
                    if (not allowedDom):
                        continue #Skip it.
                    
                    chapterName = li.text.strip()
                    
                    if (chapterURL in prevChapterLengths):
                        prevChapterLength = prevChapterLengths[chapterURL]
                    else:
                        prevChapterLength = 0
                    if (chapterURL in prevLoads):
                        prevLoad = prevLoads[chapterURL]
                    else:
                        prevLoad = 0
                    chapterDetails = make_chapter(url=chapterURL, name=chapterName, thread=chapterThread, section=sectionDisplay, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                    chapterList.append(chapterDetails)
                    save_chapters(chapterList, group)
            save_chapters(chapterList, group)
        elif (group == "peterverse"):
            #The template you gave Peterverse. It's the one with "<strong><u>Characters</u></strong>" and "<em>Collections</em>".
            #Could be slightly adapted to work with a modified sandbox ("Characters" instead being "Major thread group")
            #Happens with "Sadde (Wix) > Bluebell Flames (with Alicorn) > 1. Double Witch"
            entryContents = entry.contents
            OLs = entry.select("> ol")
            ULs = entry.select("> ul")
            OLi = 0
            ULi = 0
            sections = []
            while (OLi < len(OLs) or ULi < len(ULs)):
                if (OLi >= len(OLs)):
                    sections.extend(ULs[ULi:])
                    ULi = len(ULs)
                    continue
                elif (ULi >= len(ULs)):
                    sections.extend(OLs[OLi:])
                    OLi = len(OLs)
                    continue
                if (OLs[OLi] in ULs[ULi].find_previous_siblings("ol")):
                    sections.append(OLs[OLi])
                    OLi += 1
                    continue
                if (ULs[ULi] in OLs[OLi].find_previous_siblings("ul")):
                    sections.append(ULs[ULi])
                    ULi += 1
                    continue
                output_msg("--- ERROR ordering the lists.")
                sections = OLs
                sections.extend(ULs)
                break
            i = 0
            for section in sections:
                i += 1
                superCollection = ""
                prevStrong = section.find_previous_sibling("strong") or section.find_previous_sibling("b")
                if (prevStrong != None):
                    superCollection = get_text_on_line(prevStrong).strip()
                sectionTitle = ""
                prevEm = section.find_previous_sibling("em") or section.find_previous_sibling("i")
                if (prevStrong != None and prevEm != None):
                    prevList = None
                    if (i > 1 and i - 2 < len(sections)):
                        prevList = sections[i-2]
                    if not ((prevEm in prevStrong.find_next_siblings("em") and prevEm in prevList.find_next_siblings("em")) or (prevEm in prevStrong.find_next_siblings("i") and prevEm in prevList.find_next_siblings("i"))):
                        #It's not an "i" or an "em" both after the previous strong and after the previous list.
                        #Therefore, doesn't apply to this.
                        prevEm = None
                
                if (prevEm != None):
                    sectionTitle = ""
                    nextElement = prevEm
                    sectionTitle = get_text_on_line(prevEm).strip()
                
                sectionDisplay = superCollection + (": " if (superCollection != "" and sectionTitle != "") else "") + sectionTitle
                
                if (sectionDisplay == ""):
                    sectionDisplay = str(i)
                
                chapters = section.select("> li > a")
                if (len(chapters) < 1):
                    output_msg("Error: Couldn't find any chapters for \"{}\"".format(sectionDisplay))
                    continue
                
                output_msg("Section: {}".format(sectionDisplay))
                for link in chapters:
                    li = link.parent
                    chapterThread = get_param_in_url(link["href"], "thread") or ""
                    chapterURL = set_url_params(link["href"], **{"style": "site", "view": "flat"})
                    
                    allowedDom = False
                    for allowedDomain in allowedDomains:
                        if (chapterURL.find(allowedDomain) >= 0):
                            allowedDom = True
                    if (not allowedDom):
                        continue #Skip it.
                    
                    chapterName = li.text.strip()
                    
                    if (chapterURL in prevChapterLengths):
                        prevChapterLength = prevChapterLengths[chapterURL]
                    else:
                        prevChapterLength = 0
                    if (chapterURL in prevLoads):
                        prevLoad = prevLoads[chapterURL]
                    else:
                        prevLoad = 0
                    chapterDetails = make_chapter(url=chapterURL, name=chapterName, thread=chapterThread, section=sectionDisplay, pageCount=prevChapterLength, fullyLoaded=prevLoad)
                    chapterList.append(chapterDetails)
                    save_chapters(chapterList, group)
            save_chapters(chapterList, group)
    elif (process == "flats"):
        chapterList = load_chapters(group)
        prevChapterLengths = get_prev_chapter_lengths(group)
        
        downloaded = []
        updatedList = []
        
        output_msg("Parsing flat files (for {}).".format(group))
        output_msg("'/' means the flat page list has been done in this session. '/ Got' means the flat page list has been updated in this session.")
        
        numChapters = len(chapterList)
        chapterIndex = 0
        for chapter in chapterList:
            chapterIndex += 1
            if (not os.path.exists(chapter.path)):
                output_msg("The following chapter's first page failed to download previously.")
                chapter.path = get_file(chapter.url)
            with open(chapter.path) as chapterFirstPage:
                soup = BeautifulSoup(chapterFirstPage)
                #This could be an old version of the page. When we check if the thing's changed, we check a later page, which _will_ be the new version.
            if (not chapter.url in prevChapterLengths):
                prevChapterLengths[chapter.url] = 0
            if (soup.find("h3", class_="entry-title") == None):
                output_msg("Error finding the chapter title for {}-{} ({})".format(chapter.section, chapter.name, chapter.smallURL))
                warning = soup.find("h1", class_="text-center")
                if (warning):
                    warningExists = warning.text.find("Discretion Advised")
                    if (warningExists != None and warningExists != -1):
                        output_msg("It's due to a \"Discretion Advised\" warning.")
                        output_msg("FAILED to load chapter {}-{}.".format(chapter.section, chapter.name))
                        chapterFailed = True
                continue
            
            chapter.entryTitle = soup.find("h3", class_="entry-title").get_text()
            lessAmbiguousTitle = chapter.entryTitle
            if (re.sub("[^A-Za-z0-9_]", "", chapter.entryTitle).lower().strip() != re.sub("[^A-Za-z0-9_]", "", chapter.name).lower().strip()):
                lessAmbiguousTitle = "{} ({})".format(chapter.entryTitle, chapter.name)
            
            page_list = soup.find("div", class_="comment-page-list")
            if (page_list):
                page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
            else:
                page_count = 1
            chapter.pageCount = page_count
            origPageCount = page_count
            
            onlineURL = strip_url_fragment(chapter.url)
            
            chapterDone = False
            hasChanged = False
            beenDone = False
            chapterLastPage = None
            if (prevChapterLengths[chapter.url] != 0):
                #The previous time we loaded this chapter (since we did), it had a length (we've loaded the chapter before)
                #Now, check the latest copy of the last page in this chapter to see if it's the same length, and if it's changed
                
                chapterLastPage = set_url_params(onlineURL, **{"page": chapter.pageCount, "style":"site", "view":"flat"})
                if (chapterLastPage in downloaded):
                    #Only occurs if we've already downloaded this page in this session
                    chapterLastPagePath = get_file(chapterLastPage, replace=False)
                    beenDone = True
                else:
                    chapterLastPagePath = get_file(chapterLastPage, where="temp")
                if (not os.path.isfile(chapterLastPagePath.replace("temp/", "web_cache/"))):
                    output_msg("Failed to download the following chapter fully before.")
                else:
                    with open(chapterLastPagePath) as f1, open(chapterLastPagePath.replace("temp/", "web_cache/")) as f2:
                        soup1 = BeautifulSoup(f1)
                        soup2 = BeautifulSoup(f2)
                        
                        pageWarning = soup1.find("h1", class_="text-center")
                        if (pageWarning):
                            pageWarningExists = pageWarning.text.find("Discretion Advised")
                            if (pageWarningExists != None and pageWarningExists != -1):
                                output_msg("Found a 'Discretion Advised' warning when trying to update the copy of the following chapter.")
                                output_msg("Try manually downloading the pages before moving onto the epub creation, and this should work fine.")
                                chapterDone = True
                                chapter.fullyLoaded = 0
                        else:
                            page_list = soup1.find("div", class_="comment-page-list")
                            if (page_list):
                                page_count = int(re.match(r'Page ([0-9]+) of ([0-9]+)', page_list.find("p").text).groups()[1])
                            else:
                                page_count = 1
                            
                            if (chapter.pageCount != page_count):
                                #The first page's count is different from the "last" page's count.
                                hasChanged = True
                                chapter.pageCount = page_count
                            else:
                                #If the length has changed ("Page 35 of 35" is now "Page 35 of 37"), it'll mean #content is different
                                #If it hasn't, but a post has still been added (or edited), it'll mean #content is different
                                #If it hasn't, and #content isn't different, nothing has been modified.
                                if (str(soup1.select("div#content")[0]) == str(soup2.select("div#content")[0])):
                                    if (not chapterLastPage in downloaded):
                                        downloaded.append(chapterLastPage)
                                    onlineURL = strip_url_fragment(chapter.url)
                                    allPages = True
                                    for i in range(1, chapter.pageCount+1):
                                        pageURL = make_fileName_safe(set_url_params(onlineURL, **{"page":i, "style":"site", "view":"flat"}).replace("https://", "http://").replace("http://", "web_cache/"))
                                        if (not os.path.isfile(pageURL)):
                                            output_msg("Not all the necessary pages were loaded the last time.")
                                            allPages = False
                                    if (allPages):
                                        chapterDone = True
                                        #Content is the same, pages exist, ergo chapter's done.
                                else:
                                    hasChanged = True
                                    #The chapter page length is the same, but the content of the last page is different.
            if (not chapterDone):
                chapter.fullyLoaded = 0
                gotFirstPage = False
                downd = 0
                if (origPageCount != chapter.pageCount):
                    gotFirstPage = True
                    get_file(onlineURL)
                    downd += 1
                    if (not onlineURL in downloaded):
                        downloaded.append(onlineURL)
                    else:
                        output_msg("There's something weird going on with URL [{}]. It's reporting the wrong page count, despite us having recently downloaded it.".format(onlineURL))
                
                chapterStartPage = set_url_params(onlineURL, **{"page": 1, "style":"site", "view":"flat"})
                chapterStartPagePath = make_fileName_safe(chapterStartPage.replace("https://", "http://").replace("http://", "web_cache/"))
                if (not chapterStartPage in downloaded or not os.path.isfile(chapterStartPagePath)):
                    onlineURLPath = make_fileName_safe(onlineURL.replace("https://", "http://").replace("http://", "web_cache/"))
                    downd += 1
                    chapterStartPagePath = get_file(chapterStartPage)
                    with open(chapterStartPagePath, mode="rb") as in_file, open(onlineURLPath, mode="wb") as out_file:
                        shutil.copyfileobj(in_file, out_file)
                    downloaded.append(chapterStartPage)
                    downloaded.append(onlineURL)
                
                chapterStartPageSoup = BeautifulSoup(open(chapterStartPagePath))
                page_list = chapterStartPageSoup.find("div", class_="comment-page-list")
                if (page_list):
                    page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
                else:
                    page_count = 1
                chapter.pageCount = page_count
                
                for i in range(2, chapter.pageCount + 1): #Range skips the last number <.<
                    pageURL = set_url_params(onlineURL, **{"page":i, "style":"site", "view":"flat"})
                    if (not pageURL in downloaded):
                        downd += 1
                        get_file(pageURL)
                        downloaded.append(pageURL)
                if (downd > 0):
                    output_msg(("-"*5 + " " if hasChanged else "") + "Processed ({}/{}) \"{} > {}\": Got {} page{}.{}{}".format(chapterIndex, numChapters, shorten_long_sections(chapter.section), chapter.name, downd, "" if downd == 1 else "s", " (Including the first flat page)" if gotFirstPage else "", " (Changed)" if hasChanged else ""))
                    if (chapterLastPage and not chapterLastPage in updatedList):
                        updatedList.append(chapterLastPage)
                else:
                    output_msg("Processed ({}/{}) \"{} > {}\".{}".format(chapterIndex, numChapters, shorten_long_sections(chapter.section), chapter.name, " (Changed)" if hasChanged else ""))
            else:
                output_msg("Processed ({}/{}) \"{} > {}\".{}".format(chapterIndex, numChapters, shorten_long_sections(chapter.section), chapter.name, " /" if beenDone and chapterLastPage and not chapterLastPage in updatedList else " / Got" if beenDone else ""))
            save_chapters(chapterList, group) #Save the updated pageCount for each chapter
    elif (process == "epub"):
        output_msg("Generating epub (for {})".format(group))
        book = epub.EpubBook()
        book.set_identifier(group + "_epub")
        book.set_title(group[0].upper() + group[1:])
        book.set_language("en")
        
        with open("style.css", mode="r") as styleFile:
            style = styleFile.read()
        
        #For HTML display
        if (not os.path.isdir("output/style")):
            os.makedirs("output/style")
        with open("output/style/default.css", mode="w") as stylesheet:
            stylesheet.write(style)
        
        genericSoup = BeautifulSoup()
        
        #To be modified when adding new groups
        if (group == "pixiethreads"):
            book.add_author("Aestrix", uid="aestrix")
            book.add_author("Kappa", uid="kappa")
        elif (group == "glowfic"):
            book.add_author("Misc", uid="misc")
        elif (group == "marri"):
            book.add_author("MarriNikari", uid="marri")
            book.add_author("Misc", uid="misc")
        elif (group == "radon-absinthe"):
            book.add_author("AndaisQ", uid="andaisq")
            book.add_author("Kappa", uid="kappa")
        elif (group == "peterverse"):
            book.add_author("Peterverse", uid="peterverse")
            book.add_author("Misc", uid="misc")
        elif (group == "maggie"):
            book.add_author("Maggie", uid="maggie")
            book.add_author("Misc", uid="misc")
        else:
            book.add_author("Alicorn", uid="alicorn")
            if (group == "effulgence"):
                book.add_author("Kappa", uid="kappa")
            elif (group == "incandescence"):
                book.add_author("Aestrix", uid="aestrix")
            elif (group == "sandbox"):
                book.add_author("Misc", uid="misc")
        
        css = epub.EpubItem(uid="style_default", file_name="style/default.css", media_type="text/css", content=style)
        book.add_item(css)
        
        for fontName in os.listdir("fonts"):
            idFriendly = re.sub("[^A-Za-z0-9_]", "", fontName.lower().replace(".ttf", "").replace(".woff", ""))
            font = epub.EpubItem(uid="font_" + idFriendly, file_name="fonts/" + fontName)
            with open("fonts/" + fontName, mode="rb") as f:
                font.set_content(f.read())
            book.add_item(font)
            output_msg("Added a font [{}] (id: {})".format(fontName, "font_" + idFriendly))
        
        collectionList = get_collections(collectionList)
        
        spine = ["nav"]
        
        chapterList = load_chapters(group)
        
        cache = {"images":{}, "faces":{}, "chapters":{}, "posts":{}, "collections":collectionList, "prevImageCount":0, "prevCategory":[], "prevMajorTitle":"", "toc":[], "book":book, "process":process, "group":group, "failedMoieties":[], "failedFaces":[], "iconPages":{}, "fixedFaces":[], "fixedMoieties":[], "showAuthors":showAuthors}
        
        cache["chapterTemplate"] = pyratemp.Template(filename="chapterTemplate.pyt")
        cache["commentTemplate"] = pyratemp.Template(filename="commentTemplate.pyt")
        numChapters = len(chapterList)
        chapterIndex = 0
        
        output_msg("A tilde (~) means \"copied from previous time\".")
        output_msg("To re-generate, delete the file from output/ or use 'clean' first.")
        for chapter in chapterList:
            chapterIndex += 1
            cache["prevImageCount"] = len(cache["images"])
            
            cache["chapters"][chapter.url + "#" + chapter.thread] = chapter
            
            output_msg("Adding chapter {}/{}: {}".format(chapterIndex, numChapters, chapter.name if chapter.section == "" else "{} - {}".format(shorten_long_sections(chapter.section), chapter.name)), maintainLine=True)
            
            html = None
            fromPrev = False
            if (not os.path.exists(chapter.path)):
                output_msg("--- Chapter doesn't exist at {}. Skipping. ---".format(chapter.path))
                continue
            
            temp = do_chapter_html(chapter, chapter.path, cache)
            if (temp != None):
                html, fromPrev = temp
            if (html == None):
                output_msg("--- No HTML generated for chapter. Skipping. ---")
                continue
            
            newPageData = BeautifulSoup(html)
            if (not fromPrev):
                filePath = "output/{}-{}.html".format(cache["group"], strip_url_fragment(chapter.path).replace("web_cache/", "").split(make_fileName_safe("?"))[0].replace(".html", "") + ("~-~" + chapter.thread if chapter.thread != "" else ""))
                dir = "/".join(filePath.split("/")[:-1])
                if (dir != "." and not os.path.isdir(dir)):
                    os.makedirs(dir)
                with open(filePath, "w") as savedHTML:
                    savedHTML.write(str(newPageData))
            else:
                output_msg(" ~", maintainLine=True)
                for userBox in newPageData.select(".user"):
                    moiety = userBox.parent["class"]
                    if (not isinstance(moiety, str)):
                        moiety = " ".join(moiety)
                    oldMoiety = moiety
                    faceImgs = userBox.select(".usericon img")
                    faceDivs = userBox.select(".usericon div")
                    if (len(faceImgs) < 1 and len(faceDivs) < 1):
                        continue
                    if (len(faceImgs) > 0):
                        faceImg = faceImgs[0]
                    else:
                        faceImg = faceDivs[0]
                    faceString = faceImg.find_parent(class_="user").find(class_="username").text.strip()
                    faceStringArr = faceString.split(": ")
                    username = faceStringArr[0]
                    userID = username.replace("_", "-")
                    faceName = ": ".join(faceStringArr[1:])
                    faceName = faceName.split(" (")[0]
                    
                    faceString = userID
                    if (faceName.strip() != ""):
                        faceString += ": " + faceName
                    
                    defaultImg = ""
                    if (faceImg and "src" in faceImg.attrs and faceImg["src"].strip() != ""):
                        defaultImg = faceImg["src"]
                    face = get_face_from_string(faceString, cache, username=username, defaultImg=defaultImg)
                    
                    if (not face):
                        if (not faceString in cache["failedFaces"]):
                            cache["failedFaces"].append(faceString)
                            output_msg("--- Couldn't find a face for '{}' ---".format(faceString))
                        continue
                    
                    if (face.moiety and face.moiety.strip()):
                        moiety = face.moiety
                    
                    if (not "src" in faceImg.attrs or faceImg["src"] != face.imagePath):
                        if (face.imagePath == ""):
                            if (not faceString in cache["fixedFaces"]):
                                output_msg("--- Using the old icon for {}. ---".format(faceString))
                                cache["fixedFaces"].append(faceString)
                        else:
                            oldSrc = ""
                            if (faceImg.name == "img"):
                                oldSrc = faceImg["src"]
                                faceImg["src"] = face.imagePath
                            else:
                                newFaceImg = newPageData.new_tag("img")
                                newFaceImg["src"] = face.imagePath
                                faceImg.parent.append(newFaceImg)
                                faceImg.decompose()
                            if (not faceString in cache["fixedFaces"]):
                                cache["fixedFaces"].append(faceString)
                                output_msg("- Face {} has {} since the epub HTML was generated.\n '{}' -> '{}'".format(faceString, "changed" if oldSrc != "" else "been added", oldSrc.replace("../images/", ""), face.imagePath.replace("../images/", "")))
                    if (moiety != oldMoiety):
                        if (not userID in cache["fixedMoieties"]):
                            output_msg("- Moiety changed for user {}: '{}' -> '{}'".format(userID, oldMoiety, moiety))
                            cache["fixedMoieties"].append(userID)
                        userBox.parent["class"] = moiety
                
                for img in newPageData.select("img"):
                    if (img["src"] in cache["images"]):
                        continue
                    add_image(img["src"], cache)
            
            if (cache["prevMajorTitle"] != chapter.section):
                cache["prevMajorTitle"] = chapter.section
                
                if (len(cache["prevCategory"]) > 1):
                    cache["toc"].append(cache["prevCategory"])
                cache["prevCategory"] = [epub.Section(chapter.section), []]
            
            size = 60*1024
            if (len(str(newPageData)) > size*1.3):
                htmlData = str(newPageData.body)
                split_count = 1
                while (htmlData.strip() != ""):
                    if (len(htmlData) <= size * 1.3):
                        actualPart = htmlData
                        index = len(htmlData)
                    else:
                        index1 = htmlData[:size].lower().rfind("data-type=\"comment\"".lower())
                        index1 = size if index1 < 0 else index1
                        index = htmlData[:index1].lower().rfind("<div".lower()) #Before "data-type='comment'", find "<div"
                        index = index if index > 0 else htmlData[index1:].lower().find("<div".lower()) + index1
                        index = index if index > 0 else size
                        
                        if (index < len(htmlData)):
                            actualPart = htmlData[:index] + "<div class='contMsg'>This chapter is continued on the next page.</div>"
                        else:
                            actualPart = htmlData
                    pagePart = BeautifulSoup("<html><head></head><body>" + actualPart + "</body></html>")
                    
                    fileName = "html/" + re.sub("[^A-Za-z0-9_]", "", "{}_{}{}".format(chapter.section, chapter.name, split_count))
                    if (cache["book"].get_item_with_href(fileName + ".xhtml") != None):
                        output_msg("- Found another {}.xhtml - trying to avoid conflict.".format(fileName))
                        i = 2
                        while cache["book"].get_item_with_href(fileName + str(i) + ".xhtml") != None:
                            i += 1
                        fileName += str(i)
                        output_msg("- Solution: {}.xhtml".format(fileName))
                    fileName += ".xhtml"
                    newChapter = epub.EpubHtml(title=chapter.name + (str(split_count) if split_count != 1 else ""), file_name=fileName, lang='en')
                    newChapter.content = remove_control_characters(str(pagePart))
                    newChapter.add_link(href="../style/default.css", rel="stylesheet", type="text/css") #newChapter.add_item(css) doesn't do relative links, just does "assume we're in /EPUB/"
                    cache["book"].add_item(newChapter)
                    
                    if (split_count == 1):
                        cache["prevCategory"][1].append(newChapter)
                    spine.append(newChapter)
                    
                    htmlData = htmlData[index:]
                    split_count += 1
                output_msg("- Large chapter. {} parts created.".format(split_count-1))
            else:
                fileName = "html/" + re.sub("[^A-Za-z0-9_]", "", "{}_{}".format(chapter.section, chapter.name))
                if (cache["book"].get_item_with_href(fileName + ".xhtml") != None):
                    output_msg("- Found another {}.xhtml - trying to avoid conflict.".format(fileName))
                    i = 2
                    while cache["book"].get_item_with_href(fileName + str(i) + ".xhtml") != None:
                        i += 1
                    fileName += str(i)
                    output_msg("- Solution: {}.xhtml".format(fileName))
                fileName += ".xhtml"
                newChapter = epub.EpubHtml(title=chapter.name, file_name=fileName, lang='en')
                newChapter.content = remove_control_characters(str(newPageData))
                newChapter.add_link(href="../style/default.css", rel="stylesheet", type="text/css") #newChapter.add_item(css) doesn't do relative links, just does "assume we're in /EPUB/"
                cache["book"].add_item(newChapter)
                cache["prevCategory"][1].append(newChapter)
                spine.append(newChapter)
            chapter.fullyLoaded = chapter.pageCount #Last time, we loaded up to this point.
            newImageCount = len(cache["images"])
            if (newImageCount - cache["prevImageCount"] > 0):
                output_msg("- Added {} image{} to the book.".format(newImageCount - cache["prevImageCount"], "" if newImageCount-cache["prevImageCount"] == 1 else "s"))
            output_msg("", end="") #If we were still writing to the same line, this cancels it and starts a new line.
        if (len(cache["prevCategory"]) > 1):
            cache["toc"].append(cache["prevCategory"])
        
        cache["book"].toc = cache["toc"]
        cache["book"].add_item(epub.EpubNcx())
        
        nav = epub.EpubNav()
        nav.add_item(css)
        cache["book"].add_item(nav)
        cache["book"].spine = spine
        
        if (len(cache["failedMoieties"]) > 0):
            output_msg("--- Failed moieties:")
            for failedMoiety in cache["failedMoieties"]:
                output_msg("- {}".format(failedMoiety))
        if (len(cache["failedFaces"]) > 0):
            output_msg("--- Failed faces:")
            for failedFace in cache["failedFaces"]:
                output_msg("- {}".format(failedFace))
        if (len(cache["fixedFaces"]) > 0):
            output_msg("--- Fixed faces:")
            for fixedFace in cache["fixedFaces"]:
                output_msg("- {}".format(fixedFace))
        if (len(cache["fixedMoieties"]) > 0):
            output_msg("--- Fixed moieties:")
            for fixedMoiety in cache["fixedMoieties"]:
                output_msg("- {}".format(fixedMoiety))
        
        if (not os.path.isdir("epubs")):
            os.makedirs("epubs")
        epub.write_epub("epubs/" + group + ".epub", cache["book"], {})
        output_msg("Epub created ({}.epub).".format(group))
        save_chapters(chapterList, group)
    elif (process == "stats"):
        collectionList = get_collections(collectionList)
        
        cache = {"images":{}, "faces":{}, "chapters":{}, "posts":{}, "collections":collectionList, "prevCategory":[], "prevMajorTitle":"", "toc":[], "process":process, "group":group, "failedMoieties":[], "failedFaces":[], "iconPages":{}, "fixedFaces":[], "fixedMoieties":[], "showAuthors":showAuthors, "stats":{"entryStartDates":{}, "characterCounts":{}, "authorCounts":{}, "postWordCounts":{}}}
        
        chapterList = load_chapters(group)
        numChapters = len(chapterList)
        chapterIndex = 0
        for chapter in chapterList:
            chapterIndex += 1
            
            cache["chapters"][chapter.url + "#" + chapter.thread] = chapter
            
            output_msg("Processing chapter {}/{}: {}".format(chapterIndex, numChapters, chapter.name if chapter.section == "" else "{} - {}".format(shorten_long_sections(chapter.section), chapter.name)), maintainLine=True)
            
            if (not os.path.exists(chapter.path)):
                output_msg("--- Chapter doesn't exist at {}. Skipping. ---".format(chapter.path))
                continue
            
            process_chapter_stats(chapter, chapter.path, cache)
            
            if (cache["prevMajorTitle"] != chapter.section):
                cache["prevMajorTitle"] = chapter.section
                
                if (len(cache["prevCategory"]) > 1):
                    cache["toc"].append(cache["prevCategory"])
                cache["prevCategory"] = [epub.Section(chapter.section), []]
            output_msg("", end="")
        if (len(cache["prevCategory"]) > 1):
            cache["toc"].append(cache["prevCategory"])
        
        #Output stats here
        stats = cache["stats"]
        output_msg("-" * 10)
        output_msg("-- Threads started in each month:")
        entryStartDates = stats["entryStartDates"]
        for date in sorted(entryStartDates.keys()):
            output_msg("{}: {}".format(date, len(entryStartDates[date])))
        
        output_msg("-" * 10)
        output_msg("-- Number of posts by each character:")
        characterPostCounts = stats["characterCounts"]
        for character in sorted(characterPostCounts.keys()):
            output_msg("{}: {}".format(character, characterPostCounts[character]))
        
        output_msg("-" * 10)
        output_msg("-- Number of posts by each author:")
        authorPostCounts = stats["authorCounts"]
        for character in sorted(authorPostCounts.keys()):
            output_msg("{}: {}".format(character, authorPostCounts[character]))
        
        output_msg("-" * 10)
        output_msg("-- Number of words in each chapter (Total, #Author, ##Character):")
        postWordCounts = stats["postWordCounts"]
        for chapterShortURL in postWordCounts.keys(): #Not sorted(postWordCounts.keys()), since that's weird with longer numbers e.g. 7009 and 850.
            output_msg("--- Chapter: {}".format(chapterShortURL))
            output_msg("--- Chapter title: {}".format(postWordCounts[chapterShortURL]["chapter"].name))
            output_msg("Total: {}".format(postWordCounts[chapterShortURL]["_total"]))
            for authorMoiety in sorted(postWordCounts[chapterShortURL]["authors"].keys()):
                output_msg("#{}: {}".format(authorMoiety, postWordCounts[chapterShortURL]["authors"][authorMoiety]))
            for authorUser in sorted(postWordCounts[chapterShortURL]["users"].keys()):
                output_msg("##{}: {}".format(authorUser, postWordCounts[chapterShortURL]["users"][authorUser]))
        
        #End stats output.
    elif (process == "clean"):
        output_msg("Cleaning prior-generated HTML (for {})".format(group))
        
        chapterList = load_chapters(group)
        for chapter in chapterList:
            filePath = "output/{}-{}.html".format(cache["group"], strip_url_fragment(chapter.path).replace("web_cache/", "").split(make_fileName_safe("?"))[0].replace(".html", "") + ("~-~" + chapter.thread if chapter.thread != "" else ""))
            if (os.path.isfile(filePath)):
                os.remove(filePath)
    elif (process == "remove"):
        thing = re.sub("(^[^A-Za-z0-9]+|[^A-Za-z0-9_# *]+|[^A-Za-z0-9]+$)", "", option.replace(process, ""))
        if (thing == ""):
            output_msg("--- Please give a thing to remove! (format: e.g. alicornutopia#1640 (or alicorn*#1640) to remove the sandbox index) ---")
            return
        output_msg("Trying to clear items: {}".format(thing))
        things = thing.split(" ")
        for removeTarget in things:
            if (removeTarget.find("_") < 0 and removeTarget.find("#") < 0):
                output_msg("--- Failed to remove {} (not in the form community_ID or community#ID?) ---".format(removeTarget))
                return
            if (removeTarget.find("#") >= 0):
                community = removeTarget.split("#")[0]
                targetID = removeTarget.split("#")[1]
            elif (removeTarget.find("_") >= 0):
                community = removeTarget.split("_")[0]
                targetID = removeTarget.split("_")[1]
            if (community.find("*") >= 0):
                pos = community.find("*")
                before = community[:pos]
                after = community[pos+1:]
                dirs = []
                for dir in os.listdir("web_cache/"):
                    if ((before == "" or dir[:len(before)] == before) and (after == "" or dir[-len(after):] == after)):
                        dirs.append("web_cache/{}".format(dir))
                    else:
                        dir = dir.replace(".dreamwidth.org", "")
                        if ((before == "" or dir[:len(before)] == before) and (after == "" or dir[-len(after):] == after)):
                            dirs.append("web_cache/{}".format(dir))
                if (len(dirs) == 0):
                    output_msg("--- Failed to find any communities matching [{}] (tried [{}] before, [{}] after)".format(community, before, after))
                    return
                output_msg("Checking: ", dirs)
            else:
                dir = "web_cache/{}.dreamwidth.org".format(community)
                if (not os.path.isdir(dir)):
                    dir = "web_cache/{}".format(community)
                    if (not os.path.isdir(dir)):
                        output_msg("--- Failed to find community [{}]. ---".format(community))
                        return
                dirs = [dir]
            count = 0
            for dir in dirs:
                for file in os.listdir(dir):
                    if (file[:len(targetID + ".html")] == targetID + ".html"):
                        os.remove(dir + "/" + file)
                        count += 1
            output_msg("Cleared {} item{}.".format(count, "s" if count != 1 else ""))
    elif (process == "det"):
        output_msg("Printing list of chapters, with lengths and shortURLs.")
        chapterList = load_chapters(group)
        for chapter in chapterList:
            output_msg("{} - {} # {}; {} pages, {} loaded".format("{}>{}".format(chapter.section,chapter.name) if chapter.section != "" else chapter.name, chapter.pageCount, chapter.smallURL, chapter.pageCount, chapter.fullyLoaded))

if __name__ == '__main__':
    main(sys.argv[1:])
