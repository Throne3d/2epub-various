import cProfile
import pstats
import sys
import do_various

def main(args):
    if (len(args) < 1):
        print("Please input an argument (e.g. 'tocs_sandbox', 'flats_sandbox', 'epub_sandbox')")
        sys.exit()

    option = " ".join(args).lower().strip()
    process = ""
    group = ""
    if (option[:4] == "tocs"):
        process = "tocs"
    elif (option[:5] == "flats"):
        process = "flats"
    elif (option[:5] == "posts"):
        process = "posts"
    elif (option[:4] == "epub"):
        process = "epub"
    elif (option[:3] == "det"):
        process = "det" #Output the details of the chapters
    else:
        print("Unknown option. Please try with a valid option (call with no parameters to see some examples).")
        sys.exit()

    if (option[-10:] == "effulgence"):
        group = "effulgence"
    elif (option[-13:] == "incandescence"):
        group = "incandescence"
    elif (option[-7:] == "sandbox"):
        group = "sandbox"
    elif (option[-12:] == "pixiethreads"):
        group = "pixiethreads"
    else:
        print("Unknown thing to download. Please try with a valid option (call with no parameters to see some examples).")
        sys.exit()

    cProfile.run("do_various.main(['" + process + "_" + group + "'])", "profile.prof")

    p = pstats.Stats("profile.prof")
    p.sort_stats('cumulative').print_stats(100)
if __name__ == '__main__':
    main(sys.argv[1:])