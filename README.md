# README #

This README should document the steps necessary to setup this project in order to run the "2epub-various" script.

### What is this repository for? ###

The "2epub-various" script. It hopefully downloads the necessary chapters from dreamwidth.org appropriate to load the stories for [Effulgence](https://belltower.dreamwidth.org/8579.html) and [Incandescence](https://alicornutopia.dreamwidth.org/7441.html), as well as the [miscellaneous related sandboxes](https://alicornutopia.dreamwidth.org/1640.html), [glowfics](https://glowfic.dreamwidth.org/), [pixiethreads](https://pixiethreads.dreamwidth.org/613.html), [Marri's index](https://marrinikari.dreamwidth.org/1634.html), [radon-absinthe](https://radon-absinthe.dreamwidth.org/295.html), [the peterverse index](https://peterverse.dreamwidth.org/1643.html) and [Maggie's index](https://maggie-of-the-owls.dreamwidth.org/454.html) and then generates an epub for them.

### How do I get set up? ###
* Install [Python 3](https://www.python.org/) and "[pip](https://pip.pypa.io/en/stable/installing/)" (pip3 (`apt-get install python3-pip`) on Ubuntu) to help with installation of dependencies
* Install [BeautifulSoup 4](http://www.crummy.com/software/BeautifulSoup/) (`pip3 install beautifulsoup4`)
* Install [ebooklib](https://github.com/aerkalov/ebooklib) (`apt-get install python-ebooklib`?)
* Install the [Google Protocol Buffers](https://developers.google.com/protocol-buffers/?hl=en) standard (clone the Git repository and follow the instructions [here](https://github.com/google/protobuf/tree/master/python))
* Install [pyratemp](http://www.simple-is-better.org/template/pyratemp.html) (`pip3 install pyratemp`)
* Compile and add to path as necessary (or use the commands given for Ubuntu)
* Run runner.bat (if you're on Windows) or runner.sh (if you're on Linux) from cmd or Terminal (after chmod-ing as applicable on Linux), and follow the instructions
* Use the generated .epub file on an ebook reader

### How do I fix issues with the sandbox? ###
If it's a "discretion advised" message, it's kinda stupid, but there is currently no proper way to have the script bypass it and load the pages anyway. Currently, it's a matter of performing the following steps (replace `python3` with `python` if on Windows):

* Run `python3 do_various.py tocs_sandbox`
* Run `python3 do_various.py flats_sandbox`
* Open the applicable pages with the GET parameters `?(page=#&)style=site&view=flat` (in that order, thing in brackets when applicable, make # into numbers)
* Save the applicable pages as e.g. `web_cache/panfandom.dreamwidth.org/115923.html~×QMARK×~style=site&view=flat` (for http://panfandom.dreamwidth.org/115923.html?style=site&view=flat - make sure you replace the `?` to the `~×QMARK×~` (those aren't normal X's))
* Re-run `python3 do_various.py flats_sandbox` (it might complain about a 'Discretion Advised' message if you've done this before)
* Save the rest of the pages of the flat site (the page with the parameters `?page=1&style=site&view=flat` and then `?page=2&style=site&view=flat` (with `?` made into `~×QMARK×~` again... stupid Windows))
* (Optionally) make a backup of the files saved in the previous step (since they're so annoying to download one-by-one) (a backup as of 2015-12-24 can be found [here](https://www.dropbox.com/s/lpe84w73omv8gmh/backup-web_cache.zip?dl=0))
* Run `python3 do_various.py epub_sandbox`

This method is currently mostly tested, and should (hopefully) work. The stupid naming system (where `?` is instead `~×QMARK×~`) is to hopefully allow the thing to work on a Windows machine (since Windows disallows those characters in directories and file names)

### Who do I talk to? ###
If you have a problem, try contacting @Throne3d or another contributor to the project. Running this project is probably not for those without any programming or computing experience.